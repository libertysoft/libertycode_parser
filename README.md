LibertyCode_Parser
==================



Description
-----------

Library contains base parsing components, 
allows to get specific formatted data from specified source.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/parser ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/parser": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Parser

Parser allows to design basic parser class, 
with structure to get specific formatted data, 
from specified source.

_Elements_

- Parser

    Allows to get parsed data, from specified source,
    and to retrieve source, from specified parsed data.

- StrTableParser
    
    Extends parser features. 
    Allows to get parsed data, in array format, 
    from string source.

- PhpParser

    Extends string table parser features. 
    Allows to get parsed data, in array format, 
    from string PHP source.
    
- JsonParser

    Extends string table parser features. 
    Allows to get parsed data, in array format,
    from string JSON source.
    
- YmlParser

    Extends string table parser features. 
    Allows to get parsed data, in array format,
    from string YML source.
    
- YmlParser

    Extends string table parser features. 
    Allows to get parsed data, in array format,
    from string YML source.
    
- JsonYmlParser

    Extends YML string table parser features. 
    uses JSON formatting with comments, 
    to get parsed data.
    
- XmlParser

    Extends string table parser features. 
    Allows to get parsed data, in array format,
    from string XML source.

- DefaultXmlParser

    Extends XML string table parser features. 
    uses simple XML (without attributes), 
    to get parsed data.
    
- AttributeXmlParser

    Extends XML string table parser features. 
    uses XML with attributes, 
    to get parsed data.

- ParserFactory

    Allows to design a parser factory, 
    to provide new or specified parser instance,
    from specified configuration.
    
- StrTableParserFactory

    Extends parser factory features. 
    Provides string table parser instance.
    
_Example_

```php
// Get parser factory
use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;
$parserFactory = new StrTableParserFactory();
...
// Get new parser from configuration
$parser = $parserFactory->getObjParser(array(...));
...
// Get parsed data, from source
var_dump($parser->getData(...));
...
// Retrieve source, from parsed data
var_dump($parser->getSource(...));
...
```

#### File parser

File parser allows to design parser class, 
with structure to get specific formatted data, 
from specified source file.

_Elements_

- FileParser

    Allows to get parsed data, from specified source file,
    and to set source on file, from specified parsed data.
    Uses parser, to get data and to retrieve source.
    
- StrTableFileParser

    Extends file parser features. 
    Allows to retrieve file extension from string table parsers.
    
- FileParserFactory

    Allows to design a file parser factory, 
    to provide new or specified file parser instance,
    from specified configuration.
    Uses parser factory, to provides default file parser instance.

- StrTableFileParserFactory

    Extends file parser factory features. 
    Provides string table file parser instance.
    
_Example_

```php
...
// Get file parser factory
use liberty_code\parser\file\factory\string_table\model\StrTableFileParserFactory;
$fileParserFactory = new StrTableFileParserFactory($parserFactory);
...
// Get new file parser from configuration
$fileParser = $fileParserFactory->getObjFileParser(array(...));
...
// Get file extension
var_dump($fileParser->getStrFileExt());
...
// Get parsed data, from source on file path
var_dump($fileParser->getData('...'));
...
// Set source on file path, from parsed data
var_dump($parser->setSource('...', ...));
...
```

#### Builder

Builder allows to get parsers, from specified configuration.

_Elements_

- Builder

    Allows to get parser and file parser,
    from a specified configuration.

- FactoryBuilder

    Extends builder features. 
    Uses parser factories, to get parser and file parser.
    
_Example_

```php
...
// Get builder
use liberty_code\parser\build\factory\model\FactoryBuilder;
$builder = new FactoryBuilder($parserFactory, $fileParserFactory);
...
// Set configuration
$builder->setConfig(array(...));
...
// Get parser object
$parser = $builder->getObjParser();
...
// Get file parser object
$fileParser = $builder->getObjFileParser();
...
```

---


