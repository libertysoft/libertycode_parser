<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/parser/library/ConstParser.php');
include($strRootPath . '/src/parser/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/parser/exception/CallableInvalidFormatException.php');
include($strRootPath . '/src/parser/api/ParserInterface.php');
include($strRootPath . '/src/parser/model/DefaultParser.php');

include($strRootPath . '/src/parser/string_table/library/ConstStrTableParser.php');
include($strRootPath . '/src/parser/string_table/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/exception/SourceInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/exception/DataInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/model/StrTableParser.php');

include($strRootPath . '/src/parser/string_table/php/library/ConstPhpParser.php');
include($strRootPath . '/src/parser/string_table/php/model/PhpParser.php');

include($strRootPath . '/src/parser/string_table/json/model/JsonParser.php');

include($strRootPath . '/src/parser/string_table/yml/library/ConstYmlParser.php');
include($strRootPath . '/src/parser/string_table/yml/library/ToolBoxYmlParser.php');
include($strRootPath . '/src/parser/string_table/yml/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/yml/model/YmlParser.php');
include($strRootPath . '/src/parser/string_table/yml/model/JsonYmlParser.php');

include($strRootPath . '/src/parser/string_table/xml/library/ConstXmlParser.php');
include($strRootPath . '/src/parser/string_table/xml/library/ToolBoxXmlParser.php');
include($strRootPath . '/src/parser/string_table/xml/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/parser/string_table/xml/model/XmlParser.php');
include($strRootPath . '/src/parser/string_table/xml/model/DefaultXmlParser.php');
include($strRootPath . '/src/parser/string_table/xml/model/AttributeXmlParser.php');

include($strRootPath . '/src/parser/factory/library/ConstParserFactory.php');
include($strRootPath . '/src/parser/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/parser/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/parser/factory/api/ParserFactoryInterface.php');
include($strRootPath . '/src/parser/factory/model/DefaultParserFactory.php');

include($strRootPath . '/src/parser/factory/string_table/library/ConstStrTableParserFactory.php');
include($strRootPath . '/src/parser/factory/string_table/model/StrTableParserFactory.php');

include($strRootPath . '/src/file/library/ConstFileParser.php');
include($strRootPath . '/src/file/exception/ParserInvalidFormatException.php');
include($strRootPath . '/src/file/exception/FilePathInvalidFormatException.php');
include($strRootPath . '/src/file/api/FileParserInterface.php');
include($strRootPath . '/src/file/model/DefaultFileParser.php');

include($strRootPath . '/src/file/string_table/library/ConstStrTableFileParser.php');
include($strRootPath . '/src/file/string_table/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/file/string_table/model/StrTableFileParser.php');

include($strRootPath . '/src/file/factory/library/ConstFileParserFactory.php');
include($strRootPath . '/src/file/factory/exception/ParserFactoryInvalidFormatException.php');
include($strRootPath . '/src/file/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/file/factory/api/FileParserFactoryInterface.php');
include($strRootPath . '/src/file/factory/model/DefaultFileParserFactory.php');

include($strRootPath . '/src/file/factory/string_table/model/StrTableFileParserFactory.php');

include($strRootPath . '/src/build/library/ConstBuilder.php');
include($strRootPath . '/src/build/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/build/api/BuilderInterface.php');
include($strRootPath . '/src/build/model/DefaultBuilder.php');

include($strRootPath . '/src/build/factory/library/ConstFactoryBuilder.php');
include($strRootPath . '/src/build/factory/exception/ParserFactoryInvalidFormatException.php');
include($strRootPath . '/src/build/factory/exception/FileParserFactoryInvalidFormatException.php');
include($strRootPath . '/src/build/factory/model/FactoryBuilder.php');