<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\library;



class ConstFileParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PARSER = 'objParser';



    // Exception message constants
	const EXCEPT_MSG_PARSER_INVALID_FORMAT = 'Following parser "%1$s" invalid! It must be a parser object.';
    const EXCEPT_MSG_FILE_PATH_INVALID_FORMAT =
        'Following file path "%1$s" invalid! 
        The file path must be a valid string directory/file full path.';
}