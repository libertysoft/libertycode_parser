<?php
/**
 * Description :
 * This class allows to define default file parser.
 * Default file parser uses parser, to get data and to retrieve source.
 * Can be consider is base of all file parser type.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\parser\file\api\FileParserInterface;

use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\file\library\ConstFileParser;
use liberty_code\parser\file\exception\ParserInvalidFormatException;
use liberty_code\parser\file\exception\FilePathInvalidFormatException;



/**
 * @method ParserInterface getObjParser() Get parser object.
 * @method void setObjParser(ParserInterface $objParser) Set parser object.
 */
class DefaultFileParser extends FixBean implements FileParserInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserInterface $objParser
     */
    public function __construct(ParserInterface $objParser)
    {
        // Call parent constructor
        parent::__construct();

        // Init parser
        $this->setObjParser($objParser);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        if(!$this->beanExists(ConstFileParser::DATA_KEY_DEFAULT_PARSER))
        {
            $this->__beanTabData[ConstFileParser::DATA_KEY_DEFAULT_PARSER] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFileParser::DATA_KEY_DEFAULT_PARSER
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFileParser::DATA_KEY_DEFAULT_PARSER:
                    ParserInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Init var
        $objParser = $this->getObjParser();
        $result = $objParser->getTabConfig();

        // Return result
        return $result;
    }



    /**
     * Get string file extension,
     * from specified parser class path.
     * Overwrite it to set specific feature.
     *
     * @param string $strParserClassPath
     * @return null|string
     */
    protected function getStrFileExtEngine($strParserClassPath)
    {
        // Return result
        return null;
    }



    /**
     * @inheritdoc
     */
    public function getStrFileExt()
    {
        // Init var
        $objParser = $this->getObjParser();
        $result = $this->getStrFileExtEngine(get_class($objParser));

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws FilePathInvalidFormatException
     */
    public function getData($strFilePath)
    {
        // Set check argument
        FilePathInvalidFormatException::setCheck($strFilePath);

        // Init var
        $result = null;
        $strSrc = @file_get_contents($strFilePath);

        // Get parsed data, if required
        if($strSrc !== false)
        {
            $objParser = $this->getObjParser();
            $result = $objParser->getData($strSrc);
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        // Init var
        $objParser = $this->getObjParser();

        // Set configuration
        $objParser->setConfig($tabConfig);
    }



    /**
     * @inheritdoc
     * @throws FilePathInvalidFormatException
     */
    public function setSource($strFilePath, $data)
    {
        // Set check argument
        FilePathInvalidFormatException::setCheck($strFilePath, false);

        // Init var
        $result = false;
        $objParser = $this->getObjParser();
        $strSrc = $objParser->getSource($data);

        // Set source on file, if required
        if(!is_null($strSrc))
        {
            $result = (@file_put_contents($strFilePath, $strSrc) !== false);
        }

        // Return result
        return $result;
    }
	
	
	
}