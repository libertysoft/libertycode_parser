<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/parser/test/ParserTest.php');

// Use
use liberty_code\parser\file\string_table\model\StrTableFileParser;



// Init var
$strFilePathPhp = $strRootAppPath . '/src/parser/test/test_php.php';
$strFilePathJson = $strRootAppPath . '/src/parser/test/test_json.json';
$strFilePathYml = $strRootAppPath . '/src/parser/test/test_yml.yml';
$strFilePathXml = $strRootAppPath . '/src/parser/test/test_xml.xml';

$strFilePathSavePhp = $strRootAppPath . '/src/file/test/test_save_php.php';
$strFilePathSaveJson = $strRootAppPath . '/src/file/test/test_save_json.json';
$strFilePathSaveYml = $strRootAppPath . '/src/file/test/test_save_yml.yml';
$strFilePathSaveXml = $strRootAppPath . '/src/file/test/test_save_xml.xml';



// Test parse
echo('Test PHP parse : <br /><br />');

$objFileParser = new StrTableFileParser($objPhpParser);
/*
$objFileParser->setConfig(
    array_merge(
        $objPhpParser->getTabConfig(),
        array('php_include_require' => false)
    )
);
//*/
echo('File extension: <pre>');var_dump($objFileParser->getStrFileExt());echo('</pre>');

try{
    $data = $objFileParser->getData(null);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

$data = $objFileParser->getData($strFilePathPhp);
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

try{
    $boolSave = $objFileParser->setSource($strFilePathSavePhp, null);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

$boolSave = $objFileParser->setSource($strFilePathSavePhp, $data);
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');

echo('<br /><br /><br />');



echo('Test JSON parse : <br /><br />');

$objFileParser->setObjParser($objJsonParser);
echo('File extension: <pre>');var_dump($objFileParser->getStrFileExt());echo('</pre>');

try{
    $data = $objFileParser->getData(7);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

$data = $objFileParser->getData($strFilePathJson);
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

try{
    $boolSave = $objFileParser->setSource($strFilePathSaveJson, 7);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

$boolSave = $objFileParser->setSource($strFilePathSaveJson, $data);
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');

echo('<br /><br /><br />');



echo('Test YML parse : <br />');

$objFileParser->setObjParser($objYmlParser);
echo('File extension: <pre>');var_dump($objFileParser->getStrFileExt());echo('</pre>');

$data = $objFileParser->getData($strFilePathYml);
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$boolSave = $objFileParser->setSource($strFilePathSaveYml, $data);
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');

echo('<br /><br /><br />');



echo('Test attribute XML parse : <br />');

$objFileParser->setObjParser($objAttrXmlParser);
echo('File extension: <pre>');var_dump($objFileParser->getStrFileExt());echo('</pre>');

$data = $objFileParser->getData($strFilePathXml);
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$boolSave = $objFileParser->setSource($strFilePathSaveXml, $data);
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');

echo('<br /><br /><br />');


