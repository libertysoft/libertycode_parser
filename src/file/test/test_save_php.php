<?php 
return array (
  'test_set' => 
  array (
    'test_get' => 
    array (
      'php_t1' => 'val 1',
      'php_t2' => 
      array (
        't2_1' => 'val 2_1',
        't2_2' => '"val 2_2", t',
        't2_3' => 
        array (
          't2_3_1' => 'val 2_3_1',
          't2_3_2' => '"val 2_3_2", t',
          't2_3_3' => 
          array (
            't2_3_3_1' => 'val 2_3_3_1',
            't2_3_3_2' => '"val 2_3_3_2", t',
          ),
        ),
      ),
      'php_t3' => 
      array (
        0 => 'C:\\wamp64-3.2.3\\www\\LibertyCode\\libertycode_parser\\src\\parser\\test\\test_php.php',
        1 => 'val 3_2',
        2 => true,
        3 => 2,
      ),
    ),
  ),
);
// $test = 'test_set';