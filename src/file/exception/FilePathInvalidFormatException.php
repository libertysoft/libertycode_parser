<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\exception;

use liberty_code\parser\file\library\ConstFileParser;



class FilePathInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $filePath
     */
	public function __construct($filePath)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstFileParser::EXCEPT_MSG_FILE_PATH_INVALID_FORMAT, strval($filePath));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified file path has valid format.
	 * 
     * @param mixed $filePath
	 * @param boolean $boolExists = true
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($filePath, $boolExists = true)
    {
		// Init var
        $boolExists = (is_bool($boolExists) ? $boolExists : true);
		$result =
            // Check valid string
			is_string($filePath) &&
            (trim($filePath) != '') &&

            // Check valid parent directory
			is_dir(dirname($filePath)) &&

            // Check valid file path
			(
				(!$boolExists) ||
                is_file($filePath)
			);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($filePath);
		}
		
		// Return result
		return $result;
    }
	
	
	
}