<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\exception;

use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\file\library\ConstFileParser;



class ParserInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $parser
     */
	public function __construct($parser)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFileParser::EXCEPT_MSG_PARSER_INVALID_FORMAT,
            mb_strimwidth(strval($parser), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified parser has valid format.
	 * 
     * @param mixed $parser
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($parser)
    {
		// Init var
		$result = (
			(is_null($parser)) ||
			($parser instanceof ParserInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($parser);
		}
		
		// Return result
		return $result;
    }
	
	
	
}