<?php
/**
 * Description :
 * This class allows to describe behavior of file parser class.
 * File parser allows to get data, from specified file path,
 * and to set source on file, from specified data.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\api;



interface FileParserInterface
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string file extension.
     *
     * @return null|string
     */
    public function getStrFileExt();



    /**
     * Get parsed data,
     * from source on specified file path.
     *
     * @param string $strFilePath
     * @return null|mixed
     */
    public function getData($strFilePath);





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);



    /**
     * Set source on specified file path,
     * from specified parsed data.
     *
     * @param string $strFilePath
     * @param mixed $data
     * @return boolean
     */
    public function setSource($strFilePath, $data);
}