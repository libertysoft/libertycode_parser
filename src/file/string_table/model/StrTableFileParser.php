<?php
/**
 * Description :
 * This class allows to define string table file parser.
 * String table file parser is file parser, allows to retrieve file extension from string table parsers.
 *
 * String table file parser uses the following specified configuration:
 * [
 *     String table parser configuration,
 *
 *     php_include_require(optional: got true if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\string_table\model;

use liberty_code\parser\file\model\DefaultFileParser;

use liberty_code\parser\parser\string_table\php\library\ConstPhpParser;
use liberty_code\parser\parser\string_table\php\model\PhpParser;
use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;
use liberty_code\parser\file\string_table\library\ConstStrTableFileParser;
use liberty_code\parser\file\string_table\exception\ConfigInvalidFormatException;



class StrTableFileParser extends DefaultFileParser
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check PHP include required.
     *
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkPhpIncludeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Get
        $result = (
            (!array_key_exists(ConstStrTableFileParser::TAB_CONFIG_KEY_PHP_INCLUDE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstStrTableFileParser::TAB_CONFIG_KEY_PHP_INCLUDE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFileExtEngine($strParserClassPath)
    {
        // Init var
        $result = parent::getStrFileExtEngine($strParserClassPath); // Call parent method

        // Get file extension, if required
        switch($strParserClassPath)
        {
            case PhpParser::class:
                $result = ConstStrTableFileParser::CONFIG_FILE_EXT_PHP;
                break;

            case JsonParser::class:
                $result = ConstStrTableFileParser::CONFIG_FILE_EXT_JSON;
                break;

            case JsonYmlParser::class:
                $result = ConstStrTableFileParser::CONFIG_FILE_EXT_YML;
                break;

            case DefaultXmlParser::class:
            case AttributeXmlParser::class:
                $result = ConstStrTableFileParser::CONFIG_FILE_EXT_XML;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getData($strFilePath)
    {
        // Init var
        $result = parent::getData($strFilePath);
        $objParser = $this->getObjParser();

        // Get include PHP file return, if required
        if(
            $this->checkPhpIncludeRequired() &&
            ($objParser instanceof PhpParser) &&
            (!is_null($result))
        )
        {
            $tabData = require($strFilePath);

            // Get parsed data, from source, if required
            if(is_array($tabData))
            {
                $strSrc = var_export($tabData, true);
                $strSrc = sprintf(ConstPhpParser::CONF_PHP_FORMAT_PATTERN, $strSrc);
                $result = $objParser->getData($strSrc);
            }
        }

        // Return result
        return $result;
    }



}