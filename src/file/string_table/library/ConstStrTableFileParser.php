<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\string_table\library;



class ConstStrTableFileParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_PHP_INCLUDE_REQUIRE = 'php_include_require';

    // File extension configuration
    const CONFIG_FILE_EXT_PHP = 'php';
    const CONFIG_FILE_EXT_JSON = 'json';
    const CONFIG_FILE_EXT_YML = 'yml';
    const CONFIG_FILE_EXT_XML = 'xml';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string table file parser configuration standard.';
}