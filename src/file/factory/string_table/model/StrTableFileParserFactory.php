<?php
/**
 * Description :
 * This class allows to define string table file parser factory class.
 * String table file parser factory allows to provide and hydrate string table file parser instance.
 *
 * String table file parser factory uses the following specified configuration, to get and hydrate file parser:
 * [
 *     Default file parser factory configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\factory\string_table\model;

use liberty_code\parser\file\factory\model\DefaultFileParserFactory;

use liberty_code\parser\parser\string_table\php\model\PhpParser;
use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;
use liberty_code\parser\file\string_table\model\StrTableFileParser;



class StrTableFileParserFactory extends DefaultFileParserFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFileParserClassPathFromParserClassPath($strParserClassPath)
    {
        // Init var
        $result = null;

        // Get class path of file parser, from parser class path
        if(!is_null($strParserClassPath))
        {
            switch($strParserClassPath)
            {
                case PhpParser::class:
                case JsonParser::class:
                case JsonYmlParser::class:
                case DefaultXmlParser::class:
                case AttributeXmlParser::class:
                    $result = StrTableFileParser::class;
                    break;

                default:
                    $result = parent::getStrFileParserClassPathFromParserClassPath($strParserClassPath);
                    break;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjFileParserNewFromParser($objParser)
    {
        // Init var
        $result = null;

        // Get file parser, from parser class path
        if(!is_null($objParser))
        {
            if(
                ($objParser instanceof PhpParser) ||
                ($objParser instanceof JsonParser) ||
                ($objParser instanceof JsonYmlParser) ||
                ($objParser instanceof DefaultXmlParser) ||
                ($objParser instanceof AttributeXmlParser)
            )
            {
                $result = new StrTableFileParser($objParser);
            }
            else
            {
                $result = parent::getObjFileParserNewFromParser($objParser);
            }
        }

        // Return result
        return $result;
    }

	
	
}