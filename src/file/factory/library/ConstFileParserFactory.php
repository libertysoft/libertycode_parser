<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\factory\library;



class ConstFileParserFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PARSER_FACTORY = 'objParserFactory';
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';



    // Exception message constants
    const EXCEPT_MSG_PARSER_FACTORY_INVALID_FORMAT = 'Following parser factory "%1$s" invalid! It must be a factory object.';
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following file parser factory "%1$s" invalid! It must be null or a factory object.';
}