<?php
/**
 * Description :
 * This class allows to describe behavior of file parser factory class.
 * File parser factory allows to provide new or specified file parser instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined file parser types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\factory\api;

use liberty_code\parser\file\api\FileParserInterface;



interface FileParserFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of file parser,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrFileParserClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance file parser,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param FileParserInterface $objFileParser = null
     * @return null|FileParserInterface
     */
    public function getObjFileParser(
        array $tabConfig,
        $strConfigKey = null,
        FileParserInterface $objFileParser = null
    );
}