<?php
/**
 * Description :
 * This class allows to define default file parser factory class.
 * Default file parser factory uses parser factory, to provide and hydrate default file parser instance.
 * Can be consider is base of all file parser factory type.
 *
 * Default file parser factory uses the following specified configuration, to get and hydrate file parser:
 * [
 *     -> Configuration key(optional):
 *     ... specific parser factory configuration (@see ParserFactoryInterface )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\file\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\parser\file\factory\api\FileParserFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\parser\file\api\FileParserInterface;
use liberty_code\parser\file\model\DefaultFileParser;
use liberty_code\parser\file\factory\library\ConstFileParserFactory;
use liberty_code\parser\file\factory\exception\ParserFactoryInvalidFormatException;
use liberty_code\parser\file\factory\exception\FactoryInvalidFormatException;



/**
 * @method ParserFactoryInterface getObjParserFactory() Get parser factory object.
 * @method null|FileParserFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjParserFactory(ParserFactoryInterface $objParserFactory) Set parser factory object.
 * @method void setObjFactory(null|FileParserFactoryInterface $objFactory) Set parent factory object.
 */
class DefaultFileParserFactory extends DefaultFactory implements FileParserFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserFactoryInterface $objParserFactory
     * @param FileParserFactoryInterface $objFactory = null
     */
    public function __construct(
        ParserFactoryInterface $objParserFactory,
        FileParserFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init parser factory
        $this->setObjParserFactory($objParserFactory);

        // Init file parser factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstFileParserFactory::DATA_KEY_DEFAULT_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstFileParserFactory::DATA_KEY_DEFAULT_PARSER_FACTORY] = null;
        }

        if(!$this->beanExists(ConstFileParserFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstFileParserFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified file parser.
     *
     * @param FileParserInterface $objFileParser
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param boolean $boolIsNew = true
     */
    protected function hydrateFileParser(
        FileParserInterface $objFileParser,
        array $tabConfig,
        $strConfigKey = null,
        $boolIsNew = true
    )
    {
        // Init var
        $boolIsNew = (is_bool($boolIsNew) ? $boolIsNew : true);

        // Hydrate default file parser, if required
        if((!$boolIsNew) && ($objFileParser instanceof DefaultFileParser))
        {
            $objParser = $this->getObjParser($tabConfig, $strConfigKey);

            if(!is_null($objParser))
            {
                $objFileParser->setObjParser($objParser);
            }
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFileParserFactory::DATA_KEY_DEFAULT_PARSER_FACTORY,
            ConstFileParserFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFileParserFactory::DATA_KEY_DEFAULT_PARSER_FACTORY:
                    ParserFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstFileParserFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified configuration is valid,
     * for the specified file parser object.
     *
     * @param FileParserInterface $objFileParser
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return boolean
     */
    protected function checkConfigIsValid(
        FileParserInterface $objFileParser,
        array $tabConfig,
        $strConfigKey = null
    )
    {
        // Init var
        $strFileParserClassPath = $this->getStrFileParserClassPathEngine($tabConfig, $strConfigKey);
        $result = (
            (!is_null($strFileParserClassPath)) &&
            ($strFileParserClassPath == get_class($objFileParser))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get new object parser,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|ParserInterface
     */
    protected function getObjParser(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $objParserFactory = $this->getObjParserFactory();
        $result = $objParserFactory->getObjParser($tabConfig, $strConfigKey);

        // Return result
        return $result;
    }



    /**
     * Get parser class path,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    protected function getStrParserClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $objParserFactory = $this->getObjParserFactory();
        $result = $objParserFactory->getStrParserClassPath($tabConfig, $strConfigKey);

        // Return result
        return $result;
    }



    /**
     * Get string class path of file parser,
     * from specified parser class path.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strParserClassPath
     * @return null|string
     */
    protected function getStrFileParserClassPathFromParserClassPath($strParserClassPath)
    {
        // Init var
        $result = (
            is_null($strParserClassPath) ?
                null :
                DefaultFileParser::class
        );

        // Return result
        return $result;
    }



    /**
     * Get string class path of file parser engine,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    protected function getStrFileParserClassPathEngine(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $strParserClassPath = $this->getStrParserClassPath($tabConfig, $strConfigKey);
        $result = $this->getStrFileParserClassPathFromParserClassPath($strParserClassPath);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrFileParserClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = $this->getStrFileParserClassPathEngine($tabConfig, $strConfigKey);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrFileParserClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance file parser,
     * from specified parser.
     * Overwrite it to set specific feature.
     *
     * @param null|ParserInterface $objParser
     * @return null|FileParserInterface
     */
    protected function getObjFileParserNewFromParser($objParser)
    {
        // Init var
        $result = null;

        if(!is_null($objParser))
        {
            $result = new DefaultFileParser($objParser);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance file parser,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|FileParserInterface
     */
    protected function getObjFileParserNew(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = null;
        $strClassPath = $this->getStrFileParserClassPathEngine($tabConfig, $strConfigKey);

        // Try to create instance, from class path
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Try to create instance, if required
        if(is_null($result))
        {
            $objParser = $this->getObjParser($tabConfig, $strConfigKey);
            $result = $this->getObjFileParserNewFromParser($objParser);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance file parser engine.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param FileParserInterface $objFileParser = null
     * @return null|FileParserInterface
     */
    protected function getObjFileParserEngine(
        array $tabConfig,
        $strConfigKey = null,
        FileParserInterface $objFileParser = null
    )
    {
        // Init var
        $result = null;
        $boolIsNew = is_null($objFileParser);
        $objFileParser = (
            $boolIsNew ?
                $this->getObjFileParserNew($tabConfig, $strConfigKey) :
                $objFileParser
        );

        // Get and hydrate file parser, if required
        if(
            (!is_null($objFileParser)) &&
            $this->checkConfigIsValid($objFileParser, $tabConfig, $strConfigKey)
        )
        {
            $this->hydrateFileParser($objFileParser, $tabConfig, $strConfigKey, $boolIsNew);
            $result = $objFileParser;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjFileParser(
        array $tabConfig,
        $strConfigKey = null,
        FileParserInterface $objFileParser = null
    )
    {
        // Init var
        $result = $this->getObjFileParserEngine($tabConfig, $strConfigKey, $objFileParser);

        // Get file parser from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjFileParser($tabConfig, $strConfigKey, $objFileParser);
        }

        // Return result
        return $result;
    }



}