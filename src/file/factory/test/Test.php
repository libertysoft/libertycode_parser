<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/file/factory/test/FileParserFactoryTest.php');

// Use
use liberty_code\parser\parser\string_table\php\model\PhpParser;
// use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
// use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;
use liberty_code\parser\file\model\DefaultFileParser;
use liberty_code\parser\file\string_table\model\StrTableFileParser;



// Test new file parser
$tabFileParserData = array(
    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1,
            'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
            'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s'
        ]
    ], // Ok

    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1
        ],
        'string_table_json'
    ], // Ok

    [
        [
            'type' => 'string_table_json_yml'
        ]
    ], // Ok

    [
        [
            'type' => 'string_table_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
            'root_node_name' => 'root'
        ]
    ], // Ok

    [
        [
            'type' => 'string_table_attribute_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
        ],
        'string_table_attribute_xml_not_care'
    ], // Ok

    [
        [
            'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
            'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s'
        ],
        null,
        new DefaultFileParser(new PhpParser())
    ], // Ok

    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1
        ],
        'string_table_json',
        new DefaultFileParser(new PhpParser())
    ], // Ok

    [
        [
            'type' => 'string_table_json_yml'
        ],
        null,
        new DefaultFileParser(new JsonYmlParser())
    ], // Ok

    [
        [
            'type' => 'string_table_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
            'root_node_name' => 'root'
        ],
        null,
        new DefaultFileParser(new JsonYmlParser())
    ], // Ok

    [
        [
            'type' => 'string_table_attribute_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
        ],
        'string_table_attribute_xml_not care',
        new StrTableFileParser(new AttributeXmlParser())
    ] // Ok
);

foreach($tabFileParserData as $fileParserData)
{
    echo('Test new file parser: <br />');
    echo('<pre>');var_dump($fileParserData);echo('</pre>');

    try{
        $tabConfig = $fileParserData[0];
        $strConfigKey = (isset($fileParserData[1]) ? $fileParserData[1] : null);
        $objFileParser = (isset($fileParserData[2]) ? $fileParserData[2] : null);
        $objFileParser = $objFileParserFactory->getObjFileParser($tabConfig, $strConfigKey, $objFileParser);

        echo('Class path: <pre>');var_dump($objFileParserFactory->getStrFileParserClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objFileParser))
        {
            echo('File parser class path: <pre>');var_dump(get_class($objFileParser));echo('</pre>');
            echo('File parser config: <pre>');var_dump($objFileParser->getTabConfig());echo('</pre>');
            echo('File parser extension: <pre>');var_dump($objFileParser->getStrFileExt());echo('</pre>');
            if($objFileParser instanceof DefaultFileParser)
            {
                echo('File parser parser class path: <pre>');var_dump(get_class($objFileParser->getObjParser()));echo('</pre>');
            }
        }
        else
        {
            echo('File parser not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


