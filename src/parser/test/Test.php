<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/parser/test/ParserTest.php');



// Init var
$tabData = array(
    'key-1' => 'Value 1',
    'key-2' => 2,
    'key-3' => 3.7,
    'key-4' => true,
	'key-5' => [
        'key-5_1' => 'Value 5_1',
        'key-5_2' => 52,
        'key-5_3' => 5.3,
        'key-5_4' => false,
        'key-5_5' => [
            'key-5_5_1' => 'Value 5_5_1',
            55.2,
            2 => null,
            'key-5_5_4' => true
        ]
	],
	'key-6' => null,
    'key-7' => [
        'Value 7_1',
        72,
        null,
        false
    ]
);

$strPhpSrc = file_get_contents($strRootAppPath . '/src/parser/test/test_php.php');
$strJsonSrc = file_get_contents($strRootAppPath . '/src/parser/test/test_json.json');
$strYmlSrc = file_get_contents($strRootAppPath . '/src/parser/test/test_yml.yml');
$strXmlSrc = file_get_contents($strRootAppPath . '/src/parser/test/test_xml.xml');



// Test parse
echo('Test PHP parse : <br /><br />');

try{
    $objPhpParser->getData(null);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

try{
    $objPhpParser->getSource(null);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

$data = $objPhpParser->getData($strPhpSrc);
$src = $objPhpParser->getSource($data);
echo('Source: <pre>');print_r(htmlentities($strPhpSrc));echo('</pre>');
echo('Calculated source: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$data = $objPhpParser->getData($src);
echo('Calculated data, from calculated source: <pre>');print_r($data);echo('</pre>');

$src = $objPhpParser->getSource($tabData);
$data = $objPhpParser->getData($src);
echo('Calculated source, from tabData: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated tabData, from calculated source: <pre>');print_r($data);echo('</pre>');

echo('<br /><br /><br />');



echo('Test JSON parse : <br /><br />');

try{
    $objJsonParser->getData(7);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

try{
    $objJsonParser->getSource(7);
} catch (Exception $e) {
    echo(sprintf(
        'Exception: %1$s: %2$s<br /><br />',
        get_class($e),
        $e->getMessage()
    ));
}

$data = $objJsonParser->getData($strJsonSrc);
$src = $objJsonParser->getSource($data);
echo('Source: <pre>');print_r(htmlentities($strJsonSrc));echo('</pre>');
echo('Calculated source: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$data = $objJsonParser->getData($src);
echo('Calculated data, from calculated source: <pre>');print_r($data);echo('</pre>');

$src = $objJsonParser->getSource($tabData);
$data = $objJsonParser->getData($src);
echo('Calculated source, from tabData: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated tabData, from calculated source: <pre>');print_r($data);echo('</pre>');

echo('<br /><br /><br />');



echo('Test YML parse : <br />');

$data = $objYmlParser->getData($strYmlSrc);
$src = $objYmlParser->getSource($data);
echo('Source: <pre>');print_r(htmlentities($strYmlSrc));echo('</pre>');
echo('Calculated source: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$data = $objYmlParser->getData($src);
echo('Calculated data, from calculated source: <pre>');print_r($data);echo('</pre>');

$src = $objYmlParser->getSource($tabData);
$data = $objYmlParser->getData($src);
echo('Calculated source, from tabData: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated tabData, from calculated source: <pre>');print_r($data);echo('</pre>');

echo('<br /><br /><br />');



echo('Test XML parse : <br />');

$data = $objXmlParser->getData($strXmlSrc);
$src = $objXmlParser->getSource($data);
echo('Source: <pre>');print_r(htmlentities($strXmlSrc));echo('</pre>');
echo('Calculated source: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$data = $objXmlParser->getData($src);
echo('Calculated data, from calculated source: <pre>');print_r($data);echo('</pre>');

$src = $objXmlParser->getSource($tabData);
$data = $objXmlParser->getData($src);
echo('Calculated source, from tabData: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated tabData, from calculated source: <pre>');print_r($data);echo('</pre>');

echo('<br /><br /><br />');



echo('Test attribute XML parse : <br />');

$data = $objAttrXmlParser->getData($strXmlSrc);
$src = $objAttrXmlParser->getSource($data);
echo('Source: <pre>');print_r(htmlentities($strXmlSrc));echo('</pre>');
echo('Calculated source: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated data: <pre>');print_r($data);echo('</pre>');

$data = $objAttrXmlParser->getData($src);
echo('Calculated data, from calculated source: <pre>');print_r($data);echo('</pre>');

$objAttrXmlParser->setConfig(
    array(
        'source_format_set_pattern' =>
            '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL .
            '<root>' . PHP_EOL .
            '%1$s' . PHP_EOL .
            '</root>'
    )
);
$src = $objAttrXmlParser->getSource($tabData);
$data = $objAttrXmlParser->getData($src);
echo('Calculated source, from tabData: <pre>');print_r(htmlentities($src));echo('</pre>');
echo('Calculated tabData, from calculated source: <pre>');print_r($data);echo('</pre>');

echo('<br /><br /><br />');


