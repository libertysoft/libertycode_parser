<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\parser\parser\string_table\php\model\PhpParser;
use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;



// Init var
$objPhpParser = new PhpParser(
    array(
        'cache_source_require' => true,
        'cache_data_require' => 1,
        'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
        'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s'
    ),
    function($src){
        $result = $src;

        if(is_string($result))
        {
            $result .= PHP_EOL . '// $test = \'test_get\';';
        }

        return $result;
    },
    function($src){
        $result = $src;

        if(is_string($result))
        {
            $result .= PHP_EOL . '// $test = \'test_set\';';
        }

        return $result;
    },
    function($data){
        $result = $data;

        if(is_array($result))
        {
            $result = array('test_get' => $result);
        }

        return $result;
    },
    function($data){
        $result = $data;

        if(is_array($result))
        {
            $result = array('test_set' => $result);
        }

        return $result;
    }
);
$objJsonParser = new JsonParser();
$objYmlParser = new JsonYmlParser();
$objXmlParser = new DefaultXmlParser(
    array(
        'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
        'root_node_name' => 'root'
    )
);
$objAttrXmlParser = new AttributeXmlParser(
    array(
        'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
    )
);


