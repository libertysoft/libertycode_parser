<?php

//*
return array(
    'php_t1' => "val 1",
    "php_t2" => [
        "t2_1" => "val 2_1",
        "t2_2" => "\"val 2_2\", t",
        // test comment 1
        "t2_3" => [
            "t2_3_1" => "val 2_3_1",
            "t2_3_2" => "\"val 2_3_2\", t",
            "t2_3_3" => [
                "t2_3_3_1" => "val 2_3_3_1",
                "t2_3_3_2" => "\"val 2_3_3_2\", t"
            ]
        ]
    ],
    // test comment 2
    "php_t3" => [
        __FILE__,
        // test comment 3
        "val 3_2",
        true,
        2
    ]
);
//*/