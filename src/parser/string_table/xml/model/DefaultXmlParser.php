<?php
/**
 * Description :
 * This class allows to define default XML string table parser.
 * Default XML string table parser is XML string table parser, using simple XML (without attributes).
 *
 * Default XML string table parser uses the following specified configuration:
 * [
 *     XML string table parser configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\xml\model;

use liberty_code\parser\parser\string_table\xml\model\XmlParser;

use liberty_code\parser\parser\string_table\xml\library\ToolBoxXmlParser;



class DefaultXmlParser extends XmlParser
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $data
     * @return null|string
     */
    protected function getSourceEngine($data)
    {
		// Return result
		return ToolBoxXmlParser::getStrXml(
		    $data,
            false,
            $this->checkSourceEscapeRequired(),
            $this->getStrSourceRowSeparator(),
            $this->getStrSourceIndent(),
            $this->getStrSourceIndentStart()
        );
    }



    /**
     * @inheritdoc
     * @param mixed $src
     * @return null|array
     */
    protected function getDataEngine($src)
    {
		// Return result
		return ToolBoxXmlParser::getTabData(
            $src,
            false
        );
    }
	
	
	
}