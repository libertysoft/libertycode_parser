<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\xml\exception;

use liberty_code\parser\parser\string_table\xml\library\ConstXmlParser;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstXmlParser::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid source escape required option
            (
                (!isset($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE]) ||
                    is_int($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE]) ||
                    (
                        is_string($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE]) &&
                        ctype_digit($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE])
                    )
                )
            ) &&

            // Check valid source row separator
            (
                (!isset($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ROW_SEPARATOR])) ||
                is_string($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_ROW_SEPARATOR])
            ) &&

            // Check valid source indent
            (
                (!isset($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_INDENT])) ||
                is_string($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_INDENT])
            ) &&

            // Check valid source start indent
            (
                (!isset($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_INDENT_START])) ||
                is_string($config[ConstXmlParser::TAB_CONFIG_KEY_SOURCE_INDENT_START])
            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}