<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\xml\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\string_table\xml\library\ConstXmlParser;



class ToolBoxXmlParser extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ***************************************************************************************************

    /**
     * Get name formatted for data.
     *
     * @param string $strName
     * @return string|integer
     */
    protected static function getStrNameFormatData($strName)
    {
        // Init var
        $result = $strName;

        // Format numeric name, if required
        if(
            is_string($strName) &&
            preg_match(ConstXmlParser::CONF_FORMAT_REGEXP_NAME_NUMERIC, $strName, $tabMatch) &&
            (count($tabMatch) > 1)
        )
        {
            $result = intval($tabMatch[1]);
        }

        // Return result
        return $result;
    }



    /**
     * Get name formatted for source XML.
     *
     * @param string|integer $name
     * @return string
     */
    protected static function getStrNameFormatXml($name)
    {
        // Init var
        $result = $name;

        // Format numeric name, if required
        if(ctype_digit($name))
        {
            $result = sprintf(ConstXmlParser::CONF_FORMAT_STRING_NAME_NUMERIC, $name);
        }

        // Return result
        return $result;
    }



	/**
	 * Calculated string XML source,
     * from parse data (recursive).
	 * 
	 * @param array $tabData
	 * @param boolean $boolAttribute = false
     * @param boolean $boolEscape = true
	 * @param string $strRowSeparator = PHP_EOL
	 * @param string $strIndent = '    '
	 * @param string $strIndentStart = ''
     * @return null|string
	 */
	public static function getStrXml(
		array $tabData,
        $boolAttribute = false,
        $boolEscape = true,
		$strRowSeparator = PHP_EOL,
		$strIndent = '    ',
		$strIndentStart = ''
	)
    {
        // Init var
		$result = null;
        $boolAttribute = (is_bool($boolAttribute) ? $boolAttribute : false);
        $boolEscape = (is_bool($boolEscape) ? $boolEscape : true);
        $strRowSeparator = (is_string($strRowSeparator) ? $strRowSeparator : PHP_EOL);
        $strIndent = (is_string($strIndent) ? $strIndent : '    ');
        $strIndentStart = (is_string($strIndentStart) ? $strIndentStart : '');

        // Check valid node array
        if(count($tabData) > 1)
        {
            // Run all child nodes
            $result = '';
            foreach($tabData as $strNodeNm => $tabNodeData)
            {
                // Get info
                $tabNodeData = array($strNodeNm => $tabNodeData);

                // Get value
                $result .=
                    (trim($result) != '' ? $strRowSeparator : '') .
                    static::getStrXml(
                        $tabNodeData,
                        $boolAttribute,
                        $boolEscape,
                        $strRowSeparator,
                        $strIndent,
                        $strIndentStart
                    );
            }
        }
        // Check valid node
		else if(count($tabData) == 1)
		{
			// Get info
            $boolIsTab = false;
			$boolIsValue = false;
			$value = array_values($tabData)[0];
			
			// Get name
			$strNm = strval(array_keys($tabData)[0]);
			if($boolEscape)
			{
                $strNm = static::getStrNameFormatXml($strNm);
				$strNm = htmlentities($strNm);
			}
			
			// Get attributes
			$strAttr = '';
			if(
                $boolAttribute &&
				isset($value[ConstXmlParser::PARSE_DATA_KEY_ATTRIBUTE])
			)
			{
				$tabAttr = $value[ConstXmlParser::PARSE_DATA_KEY_ATTRIBUTE];
				$strAttr = static::getStrAttr($tabAttr, $boolEscape);
			}
			
			if(trim($strAttr) != '')
			{
				$strAttr = ' '.$strAttr;
			}
			
			// Get value
			if(
                $boolAttribute &&
				isset($value[ConstXmlParser::PARSE_DATA_KEY_VALUE])
			)
			{
				$value = $value[ConstXmlParser::PARSE_DATA_KEY_VALUE];
			}

			// Get value render
            $strValue = '';
			if(is_array($value))
			{
			    // Case of index array (without attributes): use main name as node name
                if(
                    ToolBoxTable::checkTabIsIndex($value) &&
                    (trim($strAttr) == '')
                )
                {
                    foreach($value as $tabNodeData)
                    {
                        // Get info
                        $tabNodeData = array($strNm => $tabNodeData);

                        // Get value
                        $strValue .=
                            (trim($strValue) != '' ? $strRowSeparator : '') .
                            static::getStrXml(
                                $tabNodeData,
                                $boolAttribute,
                                $boolEscape,
                                $strRowSeparator,
                                $strIndent,
                                $strIndentStart
                            );
                    }
                }
                // Case else: get render of node array
                else
                {
                    $strValue = static::getStrXml(
                        $value,
                        $boolAttribute,
                        $boolEscape,
                        $strRowSeparator,
                        $strIndent,
                        $strIndentStart.$strIndent
                    );

                    $boolIsTab = true;
                }
			}
			else
			{
				$strValue = strval($value);
				if($boolEscape)
				{
					$strValue = htmlentities($strValue);
				}
				$boolIsValue = true;
			}
			
			// Set result
			$result = sprintf($strIndentStart.'<%1$s%2$s />', $strNm, $strAttr);
			if(trim($strValue) != '')
			{
				// Get template
                $strTemplate = '%3$s';
                if($boolIsTab)
                {
                    $strTemplate =
                        $strIndentStart.'<%1$s%2$s>'.$strRowSeparator.
                        '%3$s'.$strRowSeparator.
                        $strIndentStart.'</%1$s>';
                }
				else if($boolIsValue)
				{
					$strTemplate = $strIndentStart . '<%1$s%2$s>%3$s</%1$s>';
				}
				
				// Get result
				$result = sprintf(
					$strTemplate, 
					$strNm, 
					$strAttr, 
					$strValue
				);
			}
		}
		
		// Return result
		return $result;
    }
	
	
	
	/*
     * Get string list of attributes,
	 * from specified attributes array.
     *
     * @param array $tabAttr
	 * @param boolean $boolEscape = true
     * @return string
     */
    protected static function getStrAttr(array $tabAttr, $boolEscape = true)
    {
        // Init var
        $result = '';
		
		// Run all attributes
		foreach($tabAttr as $strAttrNm => $strAttrValue)
		{
			// Get attribute name
			$strAttrNm = strval($strAttrNm);
			if($boolEscape)
			{
                $strAttrNm = static::getStrNameFormatXml($strAttrNm);
				$strAttrNm = htmlentities($strAttrNm);
			}
			
			// Get attribute value
			$strAttrValue = strval($strAttrValue);
			if($boolEscape)
			{
				$strAttrValue = htmlentities($strAttrValue);
			}
			
			// Set result
			$result .= 
			((trim($result) != '') ? ' ' : '').
			sprintf('%1$s="%2$s"', $strAttrNm, $strAttrValue);
		}

        // Return result
        return $result;
    }
	
	
	
	/**
	 * Get parse data,
     * from string XML source (recursive).
	 * 
	 * @param string $strXml
     * @param boolean $boolAttribute = false
     * @return null|array
	 */
	public static function getTabData($strXml, $boolAttribute = false)
    {
        // Init var
        $result = null;
        $boolAttribute = (is_bool($boolAttribute) ? $boolAttribute : false);

		// Check source valid
		if(is_string($strXml))
		{
			// Init var
			$objXml = new \SimpleXMLElement($strXml);
			$xmlInfo = array();
			$value = null;
			
			// Set attributes if needs
			if($boolAttribute)
			{
				// Get info
				$tabAttr = static::getTabAttr($objXml);
				
				// Set attributes if found
				if(is_array($tabAttr) && (count($tabAttr) > 0))
				{
					$xmlInfo[ConstXmlParser::PARSE_DATA_KEY_ATTRIBUTE] = $tabAttr;
				}
			}
			
			// Get value in case of multiple child nodes
			if(count($objXml) > 0)
			{
				// Run all child nodes
				$value = array();
                $tabNodeValuekeyInit = array();
				foreach($objXml as $strNodeNm => $objNodeValue)
				{
					// Get info
					// $strNodeNm = strval($strNodeNm);
					$nodeValue = static::getTabData(strval($objNodeValue->asXML()), $boolAttribute);

                    // Get value
                    foreach ($nodeValue as $nodeValuekey => $nodeValueValue) {
                        // Special case, node key already exists
                        if (array_key_exists($nodeValuekey, $value)) {
                            // Create index array of values, if required
                            if (!in_array($nodeValuekey, $tabNodeValuekeyInit)) {
                                $value[$nodeValuekey] = array($value[$nodeValuekey]);
                                $tabNodeValuekeyInit[] = $nodeValuekey;
                            }

                            $value[$nodeValuekey][] = $nodeValueValue;
                        }
                        // Standard case
                        else {
                            $value[$nodeValuekey] = $nodeValueValue;
                        }
                    }
				}
			}
			// Get simple value else
			else
			{
				$value = strval($objXml);
			}
			
			// Set value
			if($boolAttribute)
			{
                $xmlInfo[ConstXmlParser::PARSE_DATA_KEY_VALUE] = $value;
			} 
			else
			{
                $xmlInfo = $value;
			}
			
			// Set result (with name)
            $name = static::getStrNameFormatData($objXml->getName());
			$result = array($name => $xmlInfo);
		}

        // Return result
        return $result;
    }
	
	
	
	/*
     * Get associative array of attribute,
	 * from specified object XML node.
     *
     * @param \SimpleXMLElement $objXml
     * @return array
     */
    protected static function getTabAttr(\SimpleXMLElement $objXml)
    {
        // Init var
        $result = array();
        $tabAttr = $objXml->attributes();

		// Run all attributes
		foreach($tabAttr as $strKey => $strValue)
		{
            $name = static::getStrNameFormatData($strKey);
			$result[$name] = strval($strValue);
		}

        // Return result
        return $result;
    }
	
	
	
}