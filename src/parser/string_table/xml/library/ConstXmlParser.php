<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\xml\library;



class ConstXmlParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE = 'source_escape_require';
    const TAB_CONFIG_KEY_SOURCE_ROW_SEPARATOR  = 'source_row_separator';
    const TAB_CONFIG_KEY_SOURCE_INDENT = 'source_indent';
    const TAB_CONFIG_KEY_SOURCE_INDENT_START = 'source_indent_start';

    // Parsing configuration
	const PARSE_DATA_KEY_ATTRIBUTE = 'data_attribute';
	const PARSE_DATA_KEY_VALUE = 'data_value';

	const CONF_FORMAT_REGEXP_NAME_NUMERIC = '#^_(\d+)$#';
    const CONF_FORMAT_STRING_NAME_NUMERIC = '_%1$d';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the XML string table parser configuration standard.';
}