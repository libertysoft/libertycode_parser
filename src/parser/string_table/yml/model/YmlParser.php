<?php
/**
 * Description :
 * This class allows to define YML string table parser.
 * YML string table parser is string table parser, using YML string source.
 * Can be consider is base of all YML string table parser type.
 *
 * YML string table parser uses the following specified configuration:
 * [
 *     String table parser configuration,
 *
 *     source_escape_require(optional: got true, if not found): true / false,
 *
 *     source_row_separator(optional: got PHP_EOL, if not found):
 *         'String row separator, used on source calculation',
 *
 *     source_indent(optional: got '    ' (4 spaces), if not found):
 *         'String indent, used on source calculation',
 *
 *     source_indent_start(optional: got empty string, if not found):
 *         'String start indent, used on source calculation'
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\yml\model;

use liberty_code\parser\parser\string_table\model\StrTableParser;

use liberty_code\parser\parser\library\ConstParser;
use liberty_code\parser\parser\string_table\yml\library\ConstYmlParser;
use liberty_code\parser\parser\string_table\yml\exception\ConfigInvalidFormatException;



abstract class YmlParser extends StrTableParser
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstParser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check source escape required.
     *
     * @return boolean
     */
    protected function checkSourceEscapeRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstYmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstYmlParser::TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get row separator,
     * used on source calculation.
     *
     * @return string
     */
    protected function getStrSourceRowSeparator()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstYmlParser::TAB_CONFIG_KEY_SOURCE_ROW_SEPARATOR, $tabConfig) ?
                $tabConfig[ConstYmlParser::TAB_CONFIG_KEY_SOURCE_ROW_SEPARATOR] :
                PHP_EOL
        );

        // Return result
        return $result;
    }



    /**
     * Get indent,
     * used on source calculation.
     *
     * @return string
     */
    protected function getStrSourceIndent()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstYmlParser::TAB_CONFIG_KEY_SOURCE_INDENT, $tabConfig) ?
                $tabConfig[ConstYmlParser::TAB_CONFIG_KEY_SOURCE_INDENT] :
                '    '
        );

        // Return result
        return $result;
    }



    /**
     * Get start indent,
     * used on source calculation.
     *
     * @return string
     */
    protected function getStrSourceIndentStart()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $strIndentStart = (
            array_key_exists(ConstYmlParser::TAB_CONFIG_KEY_SOURCE_INDENT_START, $tabConfig) ?
                $tabConfig[ConstYmlParser::TAB_CONFIG_KEY_SOURCE_INDENT_START] :
                ''
        );

        // Return result
        return $result;
    }
	
	
	
}