<?php
/**
 * Description :
 * This class allows to define JSON YML string table parser.
 * JSON YML string table parser is YML string table parser, using JSON formatting with comments.
 *
 * JSON YML string table parser uses the following specified configuration:
 * [
 *     YML string table parser configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\yml\model;

use liberty_code\parser\parser\string_table\yml\library\ToolBoxYmlParser;
use liberty_code\parser\parser\string_table\yml\model\YmlParser;



class JsonYmlParser extends YmlParser
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getSourceEngine($data)
    {
        // Return result
        return ToolBoxYmlParser::getStrYml(
            $data,
            $this->checkSourceEscapeRequired(),
            $this->getStrSourceRowSeparator(),
            $this->getStrSourceIndent(),
            $this->getStrSourceIndentStart()
        );
    }



    /**
     * @inheritdoc
     */
    protected function getDataEngine($src)
    {
        // Init var
		$result = null;
        $src = ToolBoxYmlParser::getStrJsonFromYml($src);
		$tabData = json_decode($src, true);
		
		// Check decode valid
		if(is_array($tabData))
		{
			$result = $tabData;
		}
		
		// Return result
		return $result;
    }



}