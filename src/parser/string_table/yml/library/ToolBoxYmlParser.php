<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\yml\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\parser\parser\string_table\yml\library\ConstYmlParser;



class ToolBoxYmlParser extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
    // ******************************************************************************

    /**
     * Calculated string Yml source,
     * from parse data (recursive).
     *
     * @param array $tabData
     * @param boolean $boolEscape = true
     * @param string $strRowSeparator = PHP_EOL
     * @param string $strIndent = '    '
     * @param string $strIndentStart = ''
     * @param boolean &$boolParentTabIsIndex = false
     * @return null|string
     */
    public static function getStrYml(
        array $tabData,
        $boolEscape = true,
        $strRowSeparator = PHP_EOL,
        $strIndent = '    ',
        $strIndentStart = '',
        &$boolParentTabIsIndex = false
    )
    {
        // Init var
        $result = null;
        $boolEscape = (is_bool($boolEscape) ? $boolEscape : true);
        $strRowSeparator = (is_string($strRowSeparator) ? $strRowSeparator : PHP_EOL);
        $strIndent = (is_string($strIndent) ? $strIndent : '    ');
        $strIndentStart = (is_string($strIndentStart) ? $strIndentStart : '');
        $boolParentTabIsIndex = is_bool($boolParentTabIsIndex) ? $boolParentTabIsIndex : false;

        // Check valid node array
        if(count($tabData) > 1)
        {
            // Run all child nodes
            $result = '';
            $boolParentTabIsIndex = ToolBoxTable::checkTabIsIndex($tabData);
            foreach($tabData as $itemKey => $itemData)
            {
                // Get info
                $tabItemInfo = array($itemKey => $itemData);

                // Get value
                $result .=
                    (
                        (trim($result) != '') ?
                            (
                                ($strIndentStart != '') ?
                                    ConstYmlParser::CONF_YML_FORMAT_DATA_SEPARATOR :
                                    ''
                            ) . $strRowSeparator :
                            ''
                    ) .
                    static::getStrYml(
                        $tabItemInfo,
                        $boolEscape,
                        $strRowSeparator,
                        $strIndent,
                        $strIndentStart,
                        $boolParentTabIsIndex
                    );
            }
        }
        // Check valid node
        else if(count($tabData) == 1)
        {
            // Get info
            // $boolIndentValue = false;
            $value = array_values($tabData)[0];

            // Get name
            $nm = array_keys($tabData)[0];

            // Get value
            // $strValue = '';
            if(is_array($value))
            {
                $strValue = static::getStrYml(
                    $value,
                    $boolEscape,
                    $strRowSeparator,
                    $strIndent,
                    $strIndentStart.$strIndent,
                    $boolTabIsIndex
                );

                // Format value
                $strTabOpen = (
                    $boolTabIsIndex ?
                        ConstYmlParser::CONF_YML_FORMAT_INDEX_ARRAY_OPEN :
                        ConstYmlParser::CONF_YML_FORMAT_ASSOC_ARRAY_OPEN
                );
                $strTabClose = (
                    $boolTabIsIndex ?
                        ConstYmlParser::CONF_YML_FORMAT_INDEX_ARRAY_CLOSE :
                        ConstYmlParser::CONF_YML_FORMAT_ASSOC_ARRAY_CLOSE
                );
                $strValue =
                    $strTabOpen . $strRowSeparator .
                    $strValue . $strRowSeparator .
                    $strIndentStart . $strTabClose;
            }
            // Case of simple value
            else
            {
                // Get value
                $strValue = $value;
                if($boolEscape)
                {
                    $strValue = json_encode($value);
                }
                // $boolIndentValue = true;
            }

            // Set result
            $result = (
                (is_int($nm) && $boolParentTabIsIndex) ?
                    sprintf($strIndentStart . '%1$s', $strValue) :
                    sprintf($strIndentStart . '%1$s: %2$s', strval($nm), $strValue)
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get index array elements,
     * where no require data separator after.
     *
     * @return array
     */
    protected static function getTabNoDataSeparatorAfter()
    {
        // Init var
        $result = array(
            ConstYmlParser::CONF_YML_FORMAT_DATA_SEPARATOR,
            ConstYmlParser::CONF_YML_FORMAT_INDEX_ARRAY_OPEN,
            ConstYmlParser::CONF_YML_FORMAT_ASSOC_ARRAY_OPEN
        );

        // Return result
        return $result;
    }



    /**
     * Get index array elements,
     * where no require data separator before.
     *
     * @return array
     */
    protected static function getTabNoDataSeparatorBefore()
    {
        // Init var
        $result = array(
            ConstYmlParser::CONF_YML_FORMAT_INDEX_ARRAY_CLOSE,
            ConstYmlParser::CONF_YML_FORMAT_ASSOC_ARRAY_CLOSE
        );

        // Return result
        return $result;
    }



    /**
     * Get string JSON source,
     * from specified string YML source.
     *
     * @param string $strYml
     * @return null|string
     */
    public static function getStrJsonFromYml($strYml)
    {
        // Init var
        $result = null;

        // Check source valid
        if(is_string($strYml))
        {
            // Init var
            $result = '';
            $strRowSeparator = "\n";
            $tabNoDataSeparatorAfter = static::getTabNoDataSeparatorAfter();
            $tabNoDataSeparatorBefore = static::getTabNoDataSeparatorBefore();
            $tabRow = explode($strRowSeparator, trim($strYml));
            $intRowCount = count($tabRow);

            // Run all rows
            $intCpt = 0;
            while($intCpt < $intRowCount)
            {
                // Init var
                $boolIsLast = (($intCpt + 1) >= $intRowCount);
                $strRow = $tabRow[$intCpt];
                $strRowNext = ($boolIsLast ? null : $tabRow[$intCpt + 1]);

                // Check row valid
                if(trim($strRow) != '')
                {
                    // Check row is comment
                    $boolIsComment =
                        (strlen(trim($strRow)) >= strlen(ConstYmlParser::CONF_YML_FORMAT_COMMENT)) &&
                        (
                            substr(trim($strRow), 0 , strlen(ConstYmlParser::CONF_YML_FORMAT_COMMENT)) ==
                            ConstYmlParser::CONF_YML_FORMAT_COMMENT
                        );

                    // Check data separator required
                    $strLastChar = substr(trim($strRow), strlen(trim($strRow)) - 1);
                    $strNextFirstChar = (is_null($strRowNext) ? '' : substr(trim($strRowNext), 0, 1));
                    $boolDataSeparator =
                        (!$boolIsLast) &&
                        (!in_array($strLastChar, $tabNoDataSeparatorAfter)) &&
                        (!in_array($strNextFirstChar, $tabNoDataSeparatorBefore));

                    // Check set row
                    if(!$boolIsComment)
                    {
                        // Format row : key => "key"
                        $strRow = trim($strRow);
                        if(substr($strRow, 0, 1) != '"')
                        {
                            $strRow = preg_replace('#([^":]+):(.*)#', '"$1":$2', $strRow);
                        }

                        // Build row, set in result
                        $result .=
                            ((trim($result) != '') ? $strRowSeparator : '').
                            $strRow.
                            ($boolDataSeparator ? ConstYmlParser::CONF_YML_FORMAT_DATA_SEPARATOR : '');
                    }
                }

                $intCpt++;
            }

            if(trim($result) != '')
            {
                $result = sprintf(ConstYmlParser::CONF_JSON_FORMAT_PATTERN, $result);
            }
            else
            {
                $result = ConstYmlParser::CONF_JSON_FORMAT_EMPTY;
            }
        }

        // Return result
        return $result;
    }
	
	
	
}