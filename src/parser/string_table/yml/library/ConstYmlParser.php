<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\yml\library;



class ConstYmlParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_SOURCE_ESCAPE_REQUIRE = 'source_escape_require';
    const TAB_CONFIG_KEY_SOURCE_ROW_SEPARATOR  = 'source_row_separator';
    const TAB_CONFIG_KEY_SOURCE_INDENT = 'source_indent';
    const TAB_CONFIG_KEY_SOURCE_INDENT_START = 'source_indent_start';

    // YML format configuration
    const CONF_YML_FORMAT_COMMENT = '#';
	const CONF_YML_FORMAT_DATA_SEPARATOR = ',';
    const CONF_YML_FORMAT_INDEX_ARRAY_OPEN = '[';
    const CONF_YML_FORMAT_INDEX_ARRAY_CLOSE  = ']';
    const CONF_YML_FORMAT_ASSOC_ARRAY_OPEN  = '{';
    const CONF_YML_FORMAT_ASSOC_ARRAY_CLOSE  = '}';

    // JSON configuration
    const CONF_JSON_FORMAT_EMPTY = '[]';
    const CONF_JSON_FORMAT_PATTERN = '{%s}';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the YML string table parser configuration standard.';
}