<?php
/**
 * Description :
 * This class allows to define string table parser class.
 * String table parser uses string source to get parsed data, in array format.
 * Can be consider is base of all string table parser type.
 *
 * String table parser uses the following specified configuration:
 * [
 *     Default parser configuration,
 *
 *     source_format_get_regexp(optional):
 *         'string REGEXP pattern
 *         where first match used, to get source, if possible',
 *
 *     source_format_set_pattern(optional):
 *         'string sprintf pattern,
 *         where '%1$s' or '%s' replaced by source calculation',
 *
 *     root_node_name(optional): 'String root node name'
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\model;

use liberty_code\parser\parser\model\DefaultParser;

use liberty_code\parser\parser\library\ConstParser;
use liberty_code\parser\parser\string_table\library\ConstStrTableParser;
use liberty_code\parser\parser\string_table\exception\ConfigInvalidFormatException;
use liberty_code\parser\parser\string_table\exception\SourceInvalidFormatException;
use liberty_code\parser\parser\string_table\exception\DataInvalidFormatException;



abstract class StrTableParser extends DefaultParser
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstParser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getSourceFormatGet($src)
    {
        // Init var
        $result = parent::getSourceFormatGet($src); // Call parent method
        $tabConfig = $this->getTabConfig();

        // Format source, if required
        if(
            is_string($result) &&
            isset($tabConfig[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP])
        )
        {
            $tabMatch = array();
            if(
                (preg_match(
                    $tabConfig[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP],
                        $result,
                    $tabMatch
                ) == 1) &&
                (count($tabMatch) >= 1)
            )
            {
                $result = $tabMatch[1];
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getSourceFormatSet($src)
    {
        // Init var
        $result = parent::getSourceFormatSet($src); // Call parent method
        $tabConfig = $this->getTabConfig();

        // Format source, if required
        if(
            is_string($result) &&
            isset($tabConfig[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN])
        )
        {
            $result = sprintf(
                $tabConfig[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN],
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get root node name.
     *
     * @return null|string
     */
    protected function getStrRootNodeName()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstStrTableParser::TAB_CONFIG_KEY_ROOT_NODE_NAME, $tabConfig) ?
                $tabConfig[ConstStrTableParser::TAB_CONFIG_KEY_ROOT_NODE_NAME] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getDataFormatGet($data)
    {
        // Init var
        $result = parent::getDataFormatGet($data); // Call parent method
        $strRootNodeName = $this->getStrRootNodeName();

        // Format parsed data, if required
        if(
            is_array($result) &&
            (!is_null($strRootNodeName)) &&
            array_key_exists($strRootNodeName, $result) &&
            is_array($result[$strRootNodeName])
        )
        {
            $result = $result[$strRootNodeName];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getDataFormatSet($data)
    {
        // Init var
        $result = parent::getDataFormatSet($data); // Call parent method
        $strRootNodeName = $this->getStrRootNodeName();

        // Format parsed data, if required
        if(
            is_array($result) &&
            (!is_null($strRootNodeName))
        )
        {
            $result = array(
                $strRootNodeName => $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @return null|string
     * @throws SourceInvalidFormatException
     * @throws DataInvalidFormatException
     */
    public function getSource($data)
    {
        // Set check argument
        DataInvalidFormatException::setCheck($data);

        // Init var
        $result = parent::getSource($data); // Call parent method

        // Set check result, if required
        if(!is_null($result))
        {
            SourceInvalidFormatException::setCheck($result);
        }

        // Return result
        return $result;
    }

	
	
	/**
	 * @inheritdoc
     * @return null|array
     * @throws SourceInvalidFormatException
     * @throws DataInvalidFormatException
	 */
	public function getData($src)
	{
        // Set check argument
        SourceInvalidFormatException::setCheck($src);

        // Init var
        $result = parent::getData($src); // Call parent method

        // Set check result, if required
        if(!is_null($result))
        {
            DataInvalidFormatException::setCheck($result);
        }

        // Return result
        return $result;
	}



}