<?php
/**
 * Description :
 * This class allows to define JSON string table parser.
 * JSON string table parser is string table parser, using JSON string source.
 *
 * JSON string table parser uses the following specified configuration:
 * [
 *     String table parser configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\json\model;

use liberty_code\parser\parser\string_table\model\StrTableParser;



class JsonParser extends StrTableParser
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $data
     * @return null|string
     */
    protected function getSourceEngine($data)
    {
        // Init var
        $result = null;

        // Check valid data
        if(is_array($data))
        {
            $result = json_encode($data);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param mixed $src
     * @return null|array
     */
    protected function getDataEngine($src)
    {
        // Init var
        $result = null;
        $tabData = json_decode($src, true);

        // Check valid decoding
        if(is_array($tabData))
        {
            $result = $tabData;
        }

        // Return result
        return $result;
    }
	
	
	
}