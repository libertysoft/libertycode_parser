<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\php\library;



class ConstPhpParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONF_PHP_FORMAT_PATTERN = 'return %1$s;';
}