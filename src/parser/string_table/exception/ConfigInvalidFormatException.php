<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\exception;

use liberty_code\parser\parser\string_table\library\ConstStrTableParser;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStrTableParser::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid source format get REGEXP
            (
                (!isset($config[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP])) ||
                (
                    is_string($config[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP]) &&
                    (trim($config[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP]) != '')
                )

            ) &&

            // Check valid source format set pattern
            (
                (!isset($config[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN])) ||
                (
                    is_string($config[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN]) &&
                    (trim($config[ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN]) != '')
                )

            ) &&

            // Check valid root node name
            (
                (!isset($config[ConstStrTableParser::TAB_CONFIG_KEY_ROOT_NODE_NAME])) ||
                (
                    is_string($config[ConstStrTableParser::TAB_CONFIG_KEY_ROOT_NODE_NAME]) &&
                    (trim($config[ConstStrTableParser::TAB_CONFIG_KEY_ROOT_NODE_NAME]) != '')
                )

            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}