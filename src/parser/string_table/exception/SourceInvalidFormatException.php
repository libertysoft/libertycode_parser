<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\exception;

use liberty_code\parser\parser\string_table\library\ConstStrTableParser;



class SourceInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $src
     */
	public function __construct($src)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStrTableParser::EXCEPT_MSG_SOURCE_INVALID_FORMAT,
            mb_strimwidth(strval($src), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified source has valid format.
	 * 
     * @param mixed $src
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($src)
    {
		// Init var
		$result = is_string($src);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($src);
		}
		
		// Return result
		return $result;
    }
	
	
	
}