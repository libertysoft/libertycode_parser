<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\exception;

use liberty_code\parser\parser\string_table\library\ConstStrTableParser;



class DataInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $data
     */
	public function __construct($data)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStrTableParser::EXCEPT_MSG_DATA_INVALID_FORMAT,
            mb_strimwidth(strval($data), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified parsed data has valid format.
	 * 
     * @param mixed $data
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($data)
    {
		// Init var
		$result = is_array($data);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($data);
		}
		
		// Return result
		return $result;
    }
	
	
	
}