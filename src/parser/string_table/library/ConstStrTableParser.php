<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\string_table\library;



class ConstStrTableParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP = 'source_format_get_regexp';
    const TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN = 'source_format_set_pattern';
    const TAB_CONFIG_KEY_ROOT_NODE_NAME = 'root_node_name';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the string table parser configuration standard.';
    const EXCEPT_MSG_SOURCE_INVALID_FORMAT = 'Following source "%1$s" invalid! The source must be a string value.';
    const EXCEPT_MSG_DATA_INVALID_FORMAT = 'Following parsed data "%1$s" invalid! The parsed data must be an array value.';
}