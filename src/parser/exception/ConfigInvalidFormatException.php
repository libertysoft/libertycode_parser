<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\exception;

use liberty_code\parser\parser\library\ConstParser;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstParser::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache source required option
            (
                (!isset($config[ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE]) ||
                    is_int($config[ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE]) ||
                    (
                        is_string($config[ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE]) &&
                        ctype_digit($config[ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE])
                    )
                )
            ) &&

            // Check valid cache parsed data required option
            (
                (!isset($config[ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE]) ||
                    is_int($config[ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE]) ||
                    (
                        is_string($config[ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE]) &&
                        ctype_digit($config[ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}