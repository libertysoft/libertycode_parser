<?php
/**
 * Description :
 * This class allows to define string table parser factory class.
 * String table parser factory allows to provide and hydrate string table parser instance.
 *
 * String table parser factory uses the following specified configuration, to get and hydrate parser:
 * [
 *     -> Configuration key(optional): "type"
 *     type(required): "string_table_php",
 *     PHP string table parser configuration (@see PhpParser )
 *
 *     OR
 *
 *     -> Configuration key(optional): "type"
 *     type(optional): "string_table_json",
 *     JSON string table parser configuration (@see JsonParser )
 *
 *     OR
 *
 *     -> Configuration key(optional): "type"
 *     type(required): "string_table_json_yml",
 *     JSON YML string table parser configuration (@see JsonYmlParser )
 *
 *     OR
 *
 *     -> Configuration key(optional): "type"
 *     type(required): "string_table_xml",
 *     Default XML string table parser configuration (@see DefaultXmlParser )
 *
 *     OR
 *
 *     -> Configuration key(optional): "type"
 *     type(required): "string_table_attribute_xml",
 *     Attribute XML string table parser configuration (@see AttributeXmlParser )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\factory\string_table\model;

use liberty_code\parser\parser\factory\model\DefaultParserFactory;

use liberty_code\parser\parser\string_table\php\model\PhpParser;
use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;



class StrTableParserFactory extends DefaultParserFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrParserClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of route, from type
        switch($strConfigType)
        {
            case null:
            case ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_PHP:
                $result = PhpParser::class;
                break;

            case ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON:
                $result = JsonParser::class;
                break;

            case ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON_YML:
                $result = JsonYmlParser::class;
                break;

            case ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_XML:
                $result = DefaultXmlParser::class;
                break;

            case ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_ATTR_XML:
                $result = AttributeXmlParser::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}