<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\factory\string_table\library;



class ConstStrTableParserFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE_STR_TABLE_PHP = 'string_table_php';
    const CONFIG_TYPE_STR_TABLE_JSON = 'string_table_json';
    const CONFIG_TYPE_STR_TABLE_JSON_YML = 'string_table_json_yml';
    const CONFIG_TYPE_STR_TABLE_XML = 'string_table_xml';
    const CONFIG_TYPE_STR_TABLE_ATTR_XML = 'string_table_attribute_xml';
}