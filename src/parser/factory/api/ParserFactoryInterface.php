<?php
/**
 * Description :
 * This class allows to describe behavior of parser factory class.
 * Parser factory allows to provide new or specified parser instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined parser types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\factory\api;

use liberty_code\parser\parser\api\ParserInterface;



interface ParserFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of parser,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrParserClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance parser,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param ParserInterface $objParser = null
     * @return null|ParserInterface
     */
    public function getObjParser(
        array $tabConfig,
        $strConfigKey = null,
        ParserInterface $objParser = null
    );
}