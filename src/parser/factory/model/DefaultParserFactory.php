<?php
/**
 * Description :
 * This class allows to define default parser factory class.
 * Can be consider is base of all parser factory type.
 *
 * Default parser factory uses the following specified configuration, to get and hydrate parser:
 * [
 *     -> Configuration key(optional): "type"
 *
 *     type(optional): "string constant to determine parser type",
 *
 *     ... specific parser configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\parser\parser\factory\api\ParserFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\parser\model\DefaultParser;
use liberty_code\parser\parser\exception\CallableInvalidFormatException;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\exception\FactoryInvalidFormatException;
use liberty_code\parser\parser\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|ParserFactoryInterface getObjFactory() Get parent factory object.
 *
 * @method null|callable getCallableSourceFormatGet()
 * Get callback function to get source, used before getting parsed data.
 * @see DefaultParser::getCallableSourceFormatGet()
 *
 * @method null|callable getCallableSourceFormatSet()
 * Get callback function to get source, used after getting source.
 * @see DefaultParser::getCallableSourceFormatSet()
 *
 * @method null|callable getCallableDataFormatGet()
 * Get callback function to get data, used after getting parsed data.
 * @see DefaultParser::getCallableDataFormatGet()
 *
 * @method null|callable getCallableDataFormatSet()
 * Get callback function to get data, used before getting source.
 * @see DefaultParser::getCallableDataFormatSet()
 *
 * @method void setObjFactory(null|ParserFactoryInterface $objFactory) Set parent factory object.
 *
 * @method void setCallableSourceFormatGet(null|callable $callable)
 * Set callback function to get source, used before getting parsed data.
 * @see DefaultParserFactory::getCallableSourceFormatGet()
 *
 * @method void setCallableSourceFormatSet(null|callable $callable)
 * Set callback function to get source, used after getting source.
 * @see DefaultParserFactory::getCallableSourceFormatSet()
 *
 * @method void setCallableDataFormatGet(null|callable $callable)
 * Set callback function to get data, used after getting parsed data.
 * @see DefaultParserFactory::getCallableDataFormatGet()
 *
 * @method void setCallableDataFormatSet(null|callable $callable)
 * Set callback function to get data, used before getting source.
 * @see DefaultParserFactory::getCallableDataFormatSet()
 */
abstract class DefaultParserFactory extends DefaultFactory implements ParserFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param callable $callableSourceFormatGet = null
     * @param callable $callableSourceFormatSet = null
     * @param callable $callableDataFormatGet = null
     * @param callable $callableDataFormatSet = null
     * @param ParserFactoryInterface $objFactory = null
     */
    public function __construct(
        $callableSourceFormatGet = null,
        $callableSourceFormatSet = null,
        $callableDataFormatGet = null,
        $callableDataFormatSet = null,
        ParserFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init parser factory
        $this->setObjFactory($objFactory);

        // Init callables
        $this->setCallableSourceFormatGet($callableSourceFormatGet);
        $this->setCallableSourceFormatSet($callableSourceFormatSet);
        $this->setCallableDataFormatGet($callableDataFormatGet);
        $this->setCallableDataFormatSet($callableDataFormatSet);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstParserFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstParserFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET))
        {
            $this->__beanTabData[ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET] = null;
        }

        if(!$this->beanExists(ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET))
        {
            $this->__beanTabData[ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET] = null;
        }

        if(!$this->beanExists(ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET))
        {
            $this->__beanTabData[ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET] = null;
        }

        if(!$this->beanExists(ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET))
        {
            $this->__beanTabData[ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified parser.
     * Overwrite it to set specific call hydration.
     *
     * @param ParserInterface $objParser
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateParser(ParserInterface $objParser, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstParserFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstParserFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate parser
        $objParser->setConfig($tabConfigFormat);
        if($objParser instanceof DefaultParser)
        {
            $objParser->setCallableSourceFormatGet($this->getCallableSourceFormatGet());
            $objParser->setCallableSourceFormatSet($this->getCallableSourceFormatSet());
            $objParser->setCallableDataFormatGet($this->getCallableDataFormatGet());
            $objParser->setCallableDataFormatSet($this->getCallableDataFormatSet());
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstParserFactory::DATA_KEY_DEFAULT_FACTORY,
            ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET,
            ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET,
            ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET,
            ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstParserFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET:
                case ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET:
                case ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET:
                case ConstParserFactory::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET:
                    CallableInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified parser object.
     *
     * @param ParserInterface $objParser
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(ParserInterface $objParser, array $tabConfigFormat)
    {
        // Init var
        $strParserClassPath = $this->getStrParserClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strParserClassPath)) &&
            ($strParserClassPath == get_class($objParser))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = $tabConfig;

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as type, if required
            if(!array_key_exists(ConstParserFactory::TAB_CONFIG_KEY_TYPE, $result))
            {
                $result[ConstParserFactory::TAB_CONFIG_KEY_TYPE] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstParserFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstParserFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of parser,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrParserClassPathFromType($strConfigType);



    /**
     * Get string class path of parser engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrParserClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrParserClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrParserClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrParserClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrParserClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance parser,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|ParserInterface
     */
    protected function getObjParserNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrParserClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance parser engine.
     *
     * @param array $tabConfigFormat
     * @param ParserInterface $objParser = null
     * @return null|ParserInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjParserEngine(
        array $tabConfigFormat,
        ParserInterface $objParser = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objParser = (
            is_null($objParser) ?
                $this->getObjParserNew($strConfigType) :
                $objParser
        );

        // Get and hydrate parser, if required
        if(
            (!is_null($objParser)) &&
            $this->checkConfigIsValid($objParser, $tabConfigFormat)
        )
        {
            $this->hydrateParser($objParser, $tabConfigFormat);
            $result = $objParser;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjParser(
        array $tabConfig,
        $strConfigKey = null,
        ParserInterface $objParser = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjParserEngine($tabConfigFormat, $objParser);

        // Get parser from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjParser($tabConfig, $strConfigKey, $objParser);
        }

        // Return result
        return $result;
    }



}