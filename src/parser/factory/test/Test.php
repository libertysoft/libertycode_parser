<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/parser/factory/test/ParserFactoryTest.php');

// Use
use liberty_code\parser\parser\model\DefaultParser;
use liberty_code\parser\parser\string_table\php\model\PhpParser;
use liberty_code\parser\parser\string_table\json\model\JsonParser;
use liberty_code\parser\parser\string_table\yml\model\JsonYmlParser;
use liberty_code\parser\parser\string_table\xml\model\DefaultXmlParser;
use liberty_code\parser\parser\string_table\xml\model\AttributeXmlParser;



// Test new parser
$tabParserData = array(
    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1,
            'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
            'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s'
        ]
    ], // Ok

    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1
        ],
        'string_table_json'
    ], // Ok

    [
        [
            'type' => 'string_table_json_yml'
        ]
    ], // Ok

    [
        [
            'type' => 'string_table_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
            'root_node_name' => 'root'
        ]
    ], // Ok

    [
        [
            'type' => 'string_table_attribute_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
        ],
        'string_table_attribute_xml_not_care'
    ], // Ok

    [
        [
            'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
            'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s'
        ],
        null,
        new PhpParser()
    ], // Ok

    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1
        ],
        'string_table_php',
        new JsonParser()
    ], // Ko: not found: JSON string table parser used for PHP string table parser config

    [
        [
            'cache_source_require' => true,
            'cache_data_require' => 1
        ],
        'string_table_json',
        new JsonParser()
    ], // Ok

    [
        [
            'type' => 'string_table_json'
        ],
        null,
        new JsonYmlParser()
    ], // Ko: not found: JSON YML string table parser used for JSON string table parser config

    [
        [
            'type' => 'string_table_json_yml'
        ],
        null,
        new JsonYmlParser()
    ], // Ok

    [
        [
            'type' => 'string_table_json_yml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
            'root_node_name' => 'root'
        ],
        null,
        new DefaultXmlParser()
    ], // Ko: not found: Default XML string table parser used for JSON YML string table parser config

    [
        [
            'type' => 'string_table_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
            'root_node_name' => 'root'
        ],
        null,
        new DefaultXmlParser()
    ], // Ok

    [
        [
            'type' => 'string_table_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
        ],
        'string_table_attribute_xml_not care',
        new AttributeXmlParser()
    ], // Ko: not found: Attribute XML string table parser used for default XML string table parser config

    [
        [
            'type' => 'string_table_attribute_xml',
            'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
        ],
        'string_table_attribute_xml_not care',
        new AttributeXmlParser()
    ] // Ok
);

foreach($tabParserData as $parserData)
{
    echo('Test new parser: <br />');
    echo('<pre>');var_dump($parserData);echo('</pre>');

    try{
        $tabConfig = $parserData[0];
        $strConfigKey = (isset($parserData[1]) ? $parserData[1] : null);
        $objParser = (isset($parserData[2]) ? $parserData[2] : null);
        $objParser = $objParserFactory->getObjParser($tabConfig, $strConfigKey, $objParser);

        echo('Class path: <pre>');var_dump($objParserFactory->getStrParserClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objParser))
        {
            echo('Parser class path: <pre>');var_dump(get_class($objParser));echo('</pre>');
            echo('Parser config: <pre>');var_dump($objParser->getTabConfig());echo('</pre>');

            if($objParser instanceof DefaultParser)
            {
                echo('Parser callable source format get: <pre>');
                var_dump($objParser->getCallableSourceFormatGet());
                echo('</pre>');

                echo('Parser callable source format set: <pre>');
                var_dump($objParser->getCallableSourceFormatSet());
                echo('</pre>');

                echo('Parser callable data format get: <pre>');
                var_dump($objParser->getCallableDataFormatGet());
                echo('</pre>');

                echo('Parser callable data format set: <pre>');
                var_dump($objParser->getCallableDataFormatSet());
                echo('</pre>');
            }
        }
        else
        {
            echo('Parser not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


