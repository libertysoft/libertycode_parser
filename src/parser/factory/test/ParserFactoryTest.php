<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\parser\parser\factory\string_table\model\StrTableParserFactory;



// Init var
$objParserFactory = new StrTableParserFactory(
    null,
    null,
    function($data){
        $result = $data;

        if(is_array($result))
        {
            $result = array('test_get' => $result);
        }

        return $result;
    }
);


