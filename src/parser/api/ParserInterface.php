<?php
/**
 * Description :
 * This class allows to describe behavior of parser class.
 * Parser allows to get data, from specified source,
 * and to retrieve source, from specified data.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\api;



interface ParserInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



	/**
	 * Get source,
     * from specified parsed data.
	 *
     * @param mixed $data
	 * @return null|mixed
	 */
	public function getSource($data);
	
	
	
	/**
	 * Get parsed data,
     * from specified source.
	 *
     * @param mixed $src
	 * @return null|mixed
	 */
	public function getData($src);





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}