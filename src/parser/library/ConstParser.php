<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\library;



class ConstParser
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET = 'callableSourceFormatGet';
    const DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET = 'callableSourceFormatSet';
    const DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET = 'callableDataFormatGet';
    const DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET = 'callableDataFormatSet';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE = 'cache_source_require';
    const TAB_CONFIG_KEY_CACHE_DATA_REQUIRE = 'cache_data_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default parser configuration standard.';
    const EXCEPT_MSG_CALLABLE_INVALID_FORMAT = 'Following callable "%1$s" invalid! The callable must be null or a callback function.';

}