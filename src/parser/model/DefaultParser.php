<?php
/**
 * Description :
 * This class allows to define default parser class.
 * Can be consider is base of all parser type.
 *
 * Default parser uses the following specified configuration:
 * [
 *     cache_source_require(optional: got false, if not found): true / false,
 *
 *     cache_data_require(optional: got false, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\parser\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\parser\parser\library\ConstParser;
use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\parser\exception\ConfigInvalidFormatException;
use liberty_code\parser\parser\exception\CallableInvalidFormatException;



/**
 * @method null|callable getCallableSourceFormatGet()
 * Get callback function to get source, used before getting parsed data.
 * Format: mixed function(mixed $src)
 * @method null|callable getCallableSourceFormatSet()
 * Get callback function to get source, used after getting source.
 * Format: mixed function(mixed $src)
 * @method null|callable getCallableDataFormatGet()
 * Get callback function to get data, used after getting parsed data.
 * Format: mixed function(mixed $data)
 * @method null|callable getCallableDataFormatSet()
 * Get callback function to get data, used before getting source.
 * Format: mixed function(mixed $data)
 * @method void setCallableSourceFormatGet(null|callable $callable)
 * Set callback function to get source, used before getting parsed data.
 * Format: @see DefaultParser::getCallableSourceFormatGet()
 * @method void setCallableSourceFormatSet(null|callable $callable)
 * Set callback function to get source, used after getting source.
 * Format: @see DefaultParser::getCallableSourceFormatSet()
 * @method void setCallableDataFormatGet(null|callable $callable)
 * Set callback function to get data, used after getting parsed data.
 * Format: @see DefaultParser::getCallableDataFormatGet()
 * @method void setCallableDataFormatSet(null|callable $callable)
 * Set callback function to get data, used before getting source.
 * Format: @see DefaultParser::getCallableDataFormatSet()
 */
abstract class DefaultParser extends FixBean implements ParserInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Cached source
     * @var null|mixed
     */
    protected $cacheSrc;



    /**
     * Parsed data, used for cached source calculation
     * @var null|mixed
     */
    protected $cacheSrcData;



	/**
     * Cached parsed data
     * @var null|mixed
     */
	protected $cacheData;



    /**
     * Source, used for cached parsed data calculation
     * @var null|mixed
     */
    protected $cacheDataSrc;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param callable $callableSourceFormatGet = null
     * @param callable $callableSourceFormatSet = null
     * @param callable $callableDataFormatGet = null
     * @param callable $callableDataFormatSet = null
     */
    public function __construct(
        array $tabConfig = null,
        $callableSourceFormatGet = null,
        $callableSourceFormatSet = null,
        $callableDataFormatGet = null,
        $callableDataFormatSet = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }

        // Init callables
        $this->setCallableSourceFormatGet($callableSourceFormatGet);
        $this->setCallableSourceFormatSet($callableSourceFormatSet);
        $this->setCallableDataFormatGet($callableDataFormatGet);
        $this->setCallableDataFormatSet($callableDataFormatSet);
    }





	// Methods initialize
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	protected function beanHydrateDefault()
	{
        // Init var
        $this->cacheSrc = null;
        $this->cacheSrcData = null;
        $this->cacheData = null;
        $this->cacheDataSrc = null;

		// Init bean data
		if(!$this->beanExists(ConstParser::DATA_KEY_DEFAULT_CONFIG))
		{
			$this->__beanTabData[ConstParser::DATA_KEY_DEFAULT_CONFIG] = array();
		}

        if(!$this->beanExists(ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET))
        {
            $this->__beanTabData[ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET] = null;
        }

        if(!$this->beanExists(ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET))
        {
            $this->__beanTabData[ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET] = null;
        }

        if(!$this->beanExists(ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET))
        {
            $this->__beanTabData[ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET] = null;
        }

        if(!$this->beanExists(ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET))
        {
            $this->__beanTabData[ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET] = null;
        }
	}





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstParser::DATA_KEY_DEFAULT_CONFIG,
            ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET,
            ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET,
            ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET,
            ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstParser::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
					break;

                case ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_GET:
                case ConstParser::DATA_KEY_DEFAULT_CALLABLE_SOURCE_FORMAT_SET:
                case ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_GET:
                case ConstParser::DATA_KEY_DEFAULT_CALLABLE_DATA_FORMAT_SET:
                    CallableInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}




	
	// Methods check
	// ******************************************************************************

    /**
     * Check cache source required.
     *
     * @return boolean
     */
    protected function checkCacheSourceRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = false;

        // Check formatting required
        if(array_key_exists(ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE, $tabConfig))
        {
            $result = (intval($tabConfig[ConstParser::TAB_CONFIG_KEY_CACHE_SOURCE_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if cache source exists.
     *
     * @return boolean
     */
    public function checkCacheSourceExists()
    {
        // Return result
        return (
            (!is_null($this->cacheSrc)) &&
            (!is_null($this->cacheSrcData))
        );
    }



    /**
     * Check cache parsed data required.
     *
     * @return boolean
     */
    protected function checkCacheDataRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = false;

        // Check formatting required
        if(array_key_exists(ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE, $tabConfig))
        {
            $result = (intval($tabConfig[ConstParser::TAB_CONFIG_KEY_CACHE_DATA_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if cache parsed data exists.
     *
     * @return boolean
     */
    public function checkCacheDataExists()
    {
        // Return result
        return (
            (!is_null($this->cacheData)) &&
            (!is_null($this->cacheDataSrc))
        );
    }
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstParser::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get Get formatted source (used before getting parsed data),
     * from specified source.
     *
     * @param mixed $src
     * @return mixed
     */
    protected function getSourceFormatGet($src)
    {
        // Init var
        $result = $src;
        $callable = $this->getCallableSourceFormatGet();

        // Format source, if required
        if(!is_null($callable))
        {
            $result = $callable($result);
        }

        // Return result
        return $result;
    }



    /**
     * Get Set formatted source (used after getting source),
     * from specified source.
     *
     * @param mixed $src
     * @return mixed
     */
    protected function getSourceFormatSet($src)
    {
        // Init var
        $result = $src;
        $callable = $this->getCallableSourceFormatSet();

        // Format source, if required
        if(!is_null($callable))
        {
            $result = $callable($result);
        }

        // Return result
        return $result;
    }



    /**
     * Get Get formatted parsed data (used after getting parsed data),
     * from specified parsed data.
     *
     * @param mixed $data
     * @return mixed
     */
    protected function getDataFormatGet($data)
    {
        // Init var
        $result = $data;
        $callable = $this->getCallableDataFormatGet();

        // Format parsed data, if required
        if(!is_null($callable))
        {
            $result = $callable($result);
        }

        // Return result
        return $result;
    }



    /**
     * Get Set formatted parsed data (used before getting source),
     * from specified parsed data.
     *
     * @param mixed $data
     * @return mixed
     */
    protected function getDataFormatSet($data)
    {
        // Init var
        $result = $data;
        $callable = $this->getCallableDataFormatSet();

        // Format parsed data, if required
        if(!is_null($callable))
        {
            $result = $callable($result);
        }

        // Return result
        return $result;
    }



    /**
     * Get source engine,
     * from specified parsed data.
     *
     * @param mixed $data
     * @return null|mixed
     */
    abstract protected function getSourceEngine($data);



    /**
     * @inheritdoc
     */
    public function getSource($data)
    {
        // Init var
        $result = null;
        $data = $this->getDataFormatSet($data);
        $boolCache = $this->checkCacheSourceRequired();

        // Get source from cache, if required
        if(
            $boolCache &&
            $this->checkCacheSourceExists() &&
            (
                (
                    is_object($data) &&
                    ($data == $this->cacheSrcData)
                ) ||
                (
                    (!is_object($data)) &&
                    ($data === $this->cacheSrcData)
                )
            )
        )
        {
            $result = $this->cacheSrc;
        }
        // Case else: Get source from engine
        else
        {
            $result = $this->getSourceEngine($data);

            // Format result, if required
            if(!is_null($result))
            {
                $result = $this->getSourceFormatSet($result);
            }

            // Set cache, if required
            $this->cacheSrc = null;
            $this->cacheSrcData = null;
            if($boolCache)
            {
                $this->cacheSrc = $result;
                $this->cacheSrcData = $data;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get parsed data engine,
     * from specified source.
     *
     * @param mixed $src
     * @return null|mixed
     */
    abstract protected function getDataEngine($src);

	
	
	/**
	 * @inheritdoc
	 */
	public function getData($src)
	{
	    // Init var
	    $result = null;
        $src = $this->getSourceFormatGet($src);
        $boolCache = $this->checkCacheDataRequired();

	    // Get parsed data from cache, if required
	    if(
            $boolCache &&
            $this->checkCacheDataExists() &&
            (
                (
                    is_object($src) &&
                    ($src == $this->cacheDataSrc)
                ) ||
                (
                    (!is_object($src)) &&
                    ($src === $this->cacheDataSrc)
                )
            )
        )
        {
            $result = $this->cacheData;
        }
        // Case else: Get parsed data from engine
	    else
        {
            $result = $this->getDataEngine($src);

            // Format result, if required
            if(!is_null($result))
            {
                $result = $this->getDataFormatGet($result);
            }

            // Set cache, if required
            $this->cacheData = null;
            $this->cacheDataSrc = null;
            if($boolCache)
            {
                $this->cacheData = $result;
                $this->cacheDataSrc = $src;
            }
        }

        // Return result
        return $result;
	}





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstParser::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * Remove cache source.
     */
    public function removeCacheSource()
    {
        // Reset cache, if required
        if($this->checkCacheSourceExists())
        {
            $this->cacheSrc = null;
            $this->cacheSrcData = null;
        }
    }



    /**
     * Remove cache parsed data.
     */
    public function removeCacheData()
    {
        // Reset cache, if required
        if($this->checkCacheDataExists())
        {
            $this->cacheData = null;
            $this->cacheDataSrc = null;
        }
    }



}