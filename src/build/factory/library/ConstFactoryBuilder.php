<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\build\factory\library;



class ConstFactoryBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PARSER_FACTORY = 'objParserFactory';
    const DATA_KEY_DEFAULT_FILE_PARSER_FACTORY = 'objFileParserFactory';



    // Exception message constants
    const EXCEPT_MSG_PARSER_FACTORY_INVALID_FORMAT = 'Following parser factory "%1$s" invalid! It must be a factory object.';
    const EXCEPT_MSG_FILE_PARSER_FACTORY_INVALID_FORMAT = 'Following file parser factory "%1$s" invalid! It must be a factory object.';
}