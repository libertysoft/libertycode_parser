<?php
/**
 * Description :
 * This class allows to define factory builder class.
 * Factory builder uses parser factories, to get parser and file parser.
 *
 * Default builder uses the following specified configuration, to get parsers:
 * [
 *     Default builder configuration,
 *
 *     Parser factory configuration (@see ParserFactoryInterface ),
 *
 *     File parser factory configuration (@see FileParserFactoryInterface )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\build\factory\model;

use liberty_code\parser\build\model\DefaultBuilder;

use liberty_code\parser\parser\factory\api\ParserFactoryInterface;
use liberty_code\parser\file\factory\api\FileParserFactoryInterface;
use liberty_code\parser\build\factory\library\ConstFactoryBuilder;
use liberty_code\parser\build\factory\exception\ParserFactoryInvalidFormatException;
use liberty_code\parser\build\factory\exception\FileParserFactoryInvalidFormatException;



/**
 * @method ParserFactoryInterface getObjParserFactory() Get parser factory object.
 * @method FileParserFactoryInterface getObjFileParserFactory() Get file parser factory object.
 * @method void setObjParserFactory(ParserFactoryInterface $objParserFactory) Set parser factory object.
 * @method void setObjFileParserFactory(FileParserFactoryInterface $objFileParserFactory) Set file parser factory object.
 */
class FactoryBuilder extends DefaultBuilder
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParserFactoryInterface $objParserFactory
     * @param FileParserFactoryInterface $objFileParserFactory
     */
    public function __construct(
        ParserFactoryInterface $objParserFactory,
        FileParserFactoryInterface $objFileParserFactory,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct($tabConfig);

        // Init parser factory
        $this->setObjParserFactory($objParserFactory);

        // Init file parser factory
        $this->setObjFileParserFactory($objFileParserFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstFactoryBuilder::DATA_KEY_DEFAULT_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstFactoryBuilder::DATA_KEY_DEFAULT_PARSER_FACTORY] = null;
        }

        if(!$this->beanExists(ConstFactoryBuilder::DATA_KEY_DEFAULT_FILE_PARSER_FACTORY))
        {
            $this->__beanTabData[ConstFactoryBuilder::DATA_KEY_DEFAULT_FILE_PARSER_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstFactoryBuilder::DATA_KEY_DEFAULT_PARSER_FACTORY,
            ConstFactoryBuilder::DATA_KEY_DEFAULT_FILE_PARSER_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstFactoryBuilder::DATA_KEY_DEFAULT_PARSER_FACTORY:
                    ParserFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstFactoryBuilder::DATA_KEY_DEFAULT_FILE_PARSER_FACTORY:
                    FileParserFactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getObjParserEngine()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $objParserFactory = $this->getObjParserFactory();
        $result = $objParserFactory->getObjParser($tabConfig);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjFileParserEngine()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $objFileParserFactory = $this->getObjFileParserFactory();
        $result = $objFileParserFactory->getObjFileParser($tabConfig);

        // Return result
        return $result;
    }



}