<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE = 'cache_parser_require';
    const TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE = 'cache_file_parser_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default builder configuration standard.';
}