<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/build/test/BuildFactoryTest.php');



// Test builder
$tabConfig = array(
    [
        'cache_source_require' => true,
        'cache_data_require' => 1,
        'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
        'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s'
    ], // Ok

    [
        'type' => 'string_table_json',
        'cache_source_require' => true,
        'cache_data_require' => 1
    ], // Ok

    [
        'cache_parser_require' => true,
        'cache_file_parser_require' => 1,
        'type' => 'string_table_json_yml'
    ], // Ok

    [
        'cache_parser_require' => true,
        'cache_file_parser_require' => 1,
        'type' => 'string_table_xml',
        'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s',
        'root_node_name' => 'root'
    ], // Ok

    [
        'cache_parser_require' => true,
        'cache_file_parser_require' => 1,
        'type' => 'string_table_attribute_xml',
        'source_format_set_pattern' => '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL . '%1$s'
    ] // Ok
);

foreach($tabConfig as $config)
{
    echo('Test builder: <br />');
    echo('<pre>');var_dump($config);echo('</pre>');

    try{
        $objBuilder->setConfig($config);

        $config = $objBuilder->getTabConfig();
        echo('Builder configuration: <pre>');var_dump($config);echo('</pre>');

        $objParser = $objBuilder->getObjParser();
        echo('Builder parser: <pre>');var_dump(get_class($objParser));echo('</pre>');

        $objFileParser = $objBuilder->getObjFileParser();
        echo('Builder file parser: <pre>');var_dump(get_class($objFileParser));echo('</pre>');

        $strFileExt = $objFileParser->getStrFileExt();
        echo('Builder file extension: <pre>');var_dump($strFileExt);echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


