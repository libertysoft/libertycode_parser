<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Can be consider is base of all builder type.
 *
 * Default builder uses the following specified configuration, to get parsers:
 * [
 *     cache_parser_require(optional: got false, if not found): true / false,
 *
 *     cache_file_parser_require(optional: got false, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\parser\build\api\BuilderInterface;

use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\file\api\FileParserInterface;
use liberty_code\parser\build\library\ConstBuilder;
use liberty_code\parser\build\exception\ConfigInvalidFormatException;



abstract class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Cache parser object
     * @var null|ParserInterface
     */
    protected $objCacheParser;



    /**
     * Cache file parser object
     * @var null|FileParserInterface
     */
    protected $objCacheFileParser;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init var
        $this->objCacheParser = null;
        $this->objCacheFileParser = null;

        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstBuilder::DATA_KEY_DEFAULT_CONFIG
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
				case ConstBuilder::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
					break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}





    // Methods check
    // ******************************************************************************

    /**
     * Check cache parser required.
     *
     * @return boolean
     */
    protected function checkCacheParserRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = false;

        // Check formatting required
        if(array_key_exists(ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE, $tabConfig))
        {
            $result = (intval($tabConfig[ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if cache parser exists.
     *
     * @return boolean
     */
    public function checkCacheParserExists()
    {
        // Return result
        return (!is_null($this->objCacheParser));
    }



    /**
     * Check cache file parser required.
     *
     * @return boolean
     */
    protected function checkCacheFileParserRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = false;

        // Check formatting required
        if(array_key_exists(ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE, $tabConfig))
        {
            $result = (intval($tabConfig[ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE]) != 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if cache file parser exists.
     *
     * @return boolean
     */
    public function checkCacheFileParserExists()
    {
        // Return result
        return (!is_null($this->objCacheFileParser));
    }




	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstBuilder::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get object parser engine.
     *
     * @return null|ParserInterface
     */
    abstract protected function getObjParserEngine();



    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    public function getObjParser()
    {
        // Init var
        $result = null;
        $boolCache = $this->checkCacheParserRequired();

        // Get parser from cache, if required
        if(
            $boolCache &&
            $this->checkCacheParserExists()
        )
        {
            $result = $this->objCacheParser;
        }
        // Case else: Get parser from engine
        else
        {
            $result = $this->getObjParserEngine();

            // Set cache, if required
            $this->objCacheParser = null;
            if($boolCache)
            {
                $this->objCacheParser = $result;
            }
        }

        // Check result
        if(is_null($result))
        {
            $tabConfig = $this->getTabConfig();
            throw new ConfigInvalidFormatException(serialize($tabConfig));
        }

        // Return result
        return $result;
    }



    /**
     * Get object file parser engine.
     *
     * @return null|FileParserInterface
     */
    abstract protected function getObjFileParserEngine();



    /**
     * @inheritdoc
     */
    public function getObjFileParser()
    {
        // Init var
        $result = null;
        $boolCache = $this->checkCacheFileParserRequired();

        // Get file parser from cache, if required
        if(
            $boolCache &&
            $this->checkCacheFileParserExists()
        )
        {
            $result = $this->objCacheFileParser;
        }
        // Case else: Get file parser from engine
        else
        {
            $result = $this->getObjFileParserEngine();

            // Set cache, if required
            $this->objCacheFileParser = null;
            if($boolCache)
            {
                $this->objCacheFileParser = $result;
            }
        }

        // Check result
        if(is_null($result))
        {
            $tabConfig = $this->getTabConfig();
            throw new ConfigInvalidFormatException(serialize($tabConfig));
        }

        // Return result
        return $result;
    }
	


	
	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        // Set property
        $this->beanSet(ConstBuilder::DATA_KEY_DEFAULT_CONFIG, $tabConfig);

        // Remove cache parsers
        $this->removeCacheParser();
        $this->removeCacheFileParser();
    }



    /**
     * Remove cache parser.
     */
    public function removeCacheParser()
    {
        // Reset cache, if required
        if($this->checkCacheParserExists())
        {
            $this->objCacheParser = null;
        }
    }



    /**
     * Remove cache file parse.
     */
    public function removeCacheFileParser()
    {
        // Reset cache, if required
        if($this->checkCacheFileParserExists())
        {
            $this->objCacheFileParser = null;
        }
    }



}