<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\build\exception;

use liberty_code\parser\build\library\ConstBuilder;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstBuilder::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache parser required option
            (
                (!isset($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE]) ||
                    is_int($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE]) ||
                    (
                        is_string($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE]) &&
                        ctype_digit($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE])
                    )
                )
            ) &&

            // Check valid cache file parser required option
            (
                (!isset($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE]) ||
                    is_int($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE]) ||
                    (
                        is_string($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE]) &&
                        ctype_digit($config[ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}