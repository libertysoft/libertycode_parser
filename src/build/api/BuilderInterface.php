<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to get parser and file parser,
 * from a specified configuration.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\parser\build\api;

use liberty_code\parser\parser\api\ParserInterface;
use liberty_code\parser\file\api\FileParserInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get object parser.
     *
     * @return ParserInterface
     */
    public function getObjParser();



    /**
     * Get object file parser.
     *
     * @return FileParserInterface
     */
    public function getObjFileParser();





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}